// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "UpgradeInfoTmp.h"
#include "ObjectHolder.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UObjectHolder : public UObject
{
	GENERATED_BODY()
	
public:

	UObjectHolder(const FObjectInitializer& ObjectInitialize);
	
	UPROPERTY()
	TArray<UUpgradeInfoTmp*> upgradeInfos;
	
};
