// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "UpgradeInfoTmp.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UUpgradeInfoTmp : public UObject
{
	GENERATED_BODY()
	
public:

	int rocks;
	int wood;
	int gold;
	
	
};
