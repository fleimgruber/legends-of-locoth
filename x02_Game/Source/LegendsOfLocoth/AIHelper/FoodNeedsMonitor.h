// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ResourceNeedsMonitorBase.h"
#include "FoodNeedsMonitor.generated.h"

/**
*
*/
UCLASS()
class LEGENDSOFLOCOTH_API UFoodNeedsMonitor : public UResourceNeedsMonitorBase
{
	GENERATED_BODY()

protected:
	virtual void RecalculatePriority() override;
	void Initialize(int32 level, int32 curr);
public:
	virtual void SetNewValue(int32 newValue) override;
};
