// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "AIEnumerations.generated.h"


UENUM(BlueprintType)
enum class EResourceEnum : uint8
{
	RE_Wood 	UMETA(DisplayName = "Wood"),
	RE_Gold 	UMETA(DisplayName = "Gold"),
	RE_Food		UMETA(DisplayName = "Food"),
	RE_Population UMETA(DisplayName = "Population"),
	RE_Stone	UMETA(DisplayName = "Stone")
};

UENUM(BlueprintType)
enum class EResourceObjectiveEnum : uint8
{
	RE_ImproveDrastically 	UMETA(DisplayName = "Improve Drastically"),
	RE_Improve 	UMETA(DisplayName = "Improve"),
	RE_Hold 	UMETA(DisplayName = "Hold"),
	RE_Decrease		UMETA(DisplayName = "Decrease")
};

UENUM(BlueprintType)
enum class EDiplomaticRelationsEnum : uint8
{
	RE_Todo		UMETA(DisplayName = "Todo")
};

UENUM(BlueprintType)
enum class EImportanceEnum : uint8
{
	IE_VeryImportant 	UMETA(DisplayName = "Very Important"),
	IE_Important 	UMETA(DisplayName = "Important"),
	IE_Moderate 	UMETA(DisplayName = "Moderate"),
	IE_Low		UMETA(DisplayName = "Low")
};

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UAIEnumerations : public UObject
{
	GENERATED_BODY()
	
};
