// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "EconomicObjective.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UEconomicObjective : public UObjective
{
	GENERATED_BODY()
	
public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Objective")
		EResourceEnum Resource;

		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Objective")
		EResourceObjectiveEnum Goal;

		
		
};
