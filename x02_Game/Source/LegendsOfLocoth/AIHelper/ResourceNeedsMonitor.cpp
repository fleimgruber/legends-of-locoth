// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "ResourceNeedsMonitor.h"




void UResourceNeedsMonitor::RecalculatePriority()
{
	if (priorityLevel == 0)
	{
		if (currValue >= 1200)
		{
			priority = 0;
		}
		else if (currValue >= 800)
		{
			priority = 1;
		}
		else if (currValue >= 300)
		{
			priority = 2;
		}
		else
		{
			priority = 3;
		}
	}
	else if (priorityLevel == 1)
	{
		if (currValue >= 1700)
		{
			priority = 0;
		}
		else if (currValue >= 1400)
		{
			priority = 1;
		}
		else if (currValue >= 1100)
		{
			priority = 2;
		}
		else if (currValue >= 800)
		{
			priority = 3;
		}
		else
		{
			priority = 4;
		}
	}
	else if (priorityLevel == 2)
	{
		if (currValue >= 3000)
		{
			priority = 3;
		}
		else if (currValue >= 2500)
		{
			priority = 4;
		}
		else if (currValue >= 2000)
		{
			priority = 5;
		}
		else if (currValue >= 1500)
		{
			priority = 6;
		}
		else
		{
			priority = 7;
		}
	}
	else if (priorityLevel == 3)
	{
		if (currValue >= 5500)
		{
			priority = 4;
		}
		else if (currValue >= 4500)
		{
			priority = 5;
		}
		else if (currValue >= 4000)
		{
			priority = 6;
		}
		else if (currValue >= 3500)
		{
			priority = 7;
		}
		else if (currValue >= 2750)
		{
			priority = 8;
		}
		else
		{
			priority = 9;
		}
	}
	else if (priorityLevel == 4)
	{
		if (currValue >= 7000)
		{
			priority = 5;
		}
		else if (currValue >= 6400)
		{
			priority = 6;
		}
		else if (currValue >= 5600)
		{
			priority = 7;
		}
		else if (currValue >= 4800)
		{
			priority = 8;
		}
		else if (currValue >= 4000)
		{
			priority = 9;
		}
		else
		{
			priority = 10;
		}
	}
	else
	{
		if (currValue >= 15000)
		{
			priority = 7;
		}
		else if (currValue >= 10000)
		{
			priority = 8;
		}
		else if (currValue >= 5000)
		{
			priority = 9;
		}
		else
		{
			priority = 10;
		}
	}
}

void UResourceNeedsMonitor::SetNewValue(int32 newValue)
{
	currValue = newValue;
	RecalculatePriority();
}

void UResourceNeedsMonitor::Initialize(int32 level, int32 curr)
{
	currValue = curr;
	priorityLevel = level;
	RecalculatePriority();
}