// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "PopulationNeedsMonitor.h"




void UPopulationNeedsMonitor::Initialize(int32 level, int32 curr, int32 max)
{
	currValue = curr;
	maxPopulation = max;
	priorityLevel = level;
	RecalculatePriority();
}

void UPopulationNeedsMonitor::RecalculatePriority()
{
	int32 diff = maxPopulation - currValue;
	if (priorityLevel == 0)
	{
		if (diff >= 4)
		{
			priority = 0;
		}
		else if (diff == 3)
		{
			priority = 1;
		}
		else
		{
			priority = 2;
		}
	}
	else if (priorityLevel < 4)
	{
		int32 addidionalFactorForPriorityLevel = (1 - priorityLevel) * 2;
		if (diff >= 5)
		{
			priority = 1;
		}
		else if (diff == 4)
		{
			priority = 2;
		}
		else if (diff == 3)
		{
			priority = 3;
		}
		else
		{
			priority = 4;
		}
		priority += addidionalFactorForPriorityLevel;
	}
	else if (priorityLevel == 4)
	{
		if (diff >= 5)
		{
			priority = 7;
		}
		else if (diff == 4)
		{
			priority = 8;
		}
		else if (diff == 3 || diff == 4)
		{
			priority = 9;
		}
		else
		{
			priority = 10;
		}
	}
	else
	{
		if (diff >= 5)
		{
			priority = 9;
		}
		else
		{
			priority = 10;
		}
	}
}

void UPopulationNeedsMonitor::SetNewValue(int32 newValue)
{
	currValue = newValue;
	RecalculatePriority();
}

void UPopulationNeedsMonitor::SetNewMax(int32 newMax)
{
	maxPopulation = newMax;
	RecalculatePriority();
}