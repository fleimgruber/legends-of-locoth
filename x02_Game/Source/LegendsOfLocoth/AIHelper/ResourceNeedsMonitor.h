// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ResourceNeedsMonitorBase.h"
#include "ResourceNeedsMonitor.generated.h"

/**
*
*/
UCLASS()
class LEGENDSOFLOCOTH_API UResourceNeedsMonitor : public UResourceNeedsMonitorBase
{
	GENERATED_BODY()
protected:
	virtual void RecalculatePriority() override;
public:
	void Initialize(int32 level, int32 curr);
	virtual void SetNewValue(int32 newValue) override;
};
