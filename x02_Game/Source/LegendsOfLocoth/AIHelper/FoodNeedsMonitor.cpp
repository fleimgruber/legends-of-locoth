// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "FoodNeedsMonitor.h"




void UFoodNeedsMonitor::RecalculatePriority()
{
	
}

void UFoodNeedsMonitor::SetNewValue(int32 newValue)
{
	currValue = newValue;
	RecalculatePriority();
}

void UFoodNeedsMonitor::Initialize(int32 level, int32 curr)
{
	currValue = curr;
	priorityLevel = level;
	RecalculatePriority();
}