// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "ResourceNeedsMonitorBase.h"



void UResourceNeedsMonitorBase::SetNewValue(int32 newValue)
{
}
void UResourceNeedsMonitorBase::RecalculatePriority()
{
}

int32 UResourceNeedsMonitorBase::GetPriority()
{
	return priority;
}


int32 UResourceNeedsMonitorBase::GetPriorityLevel()
{
	return priorityLevel;
}

void UResourceNeedsMonitorBase::SetPriorityLevel(int32 level)
{
	priorityLevel = level;
}