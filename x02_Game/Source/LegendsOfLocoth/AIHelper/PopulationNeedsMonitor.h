// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ResourceNeedsMonitorBase.h"
#include "PopulationNeedsMonitor.generated.h"

/**
*
*/
UCLASS()
class LEGENDSOFLOCOTH_API UPopulationNeedsMonitor : public UResourceNeedsMonitorBase
{
	GENERATED_BODY()
protected:
	virtual void RecalculatePriority() override;
	int32 maxPopulation;

public:
	virtual void SetNewValue(int32 newValue) override;
	void Initialize(int32 level, int32 curr, int32 max);
	void SetNewMax(int32 newMax);
};
