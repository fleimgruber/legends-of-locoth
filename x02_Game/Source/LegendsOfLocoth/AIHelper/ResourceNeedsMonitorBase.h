// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "ResourceNeedsMonitorBase.generated.h"

/**
*
*/
UCLASS()
class LEGENDSOFLOCOTH_API UResourceNeedsMonitorBase : public UObject
{
	GENERATED_BODY()
protected:

	int32 lastValue;
	int32 currValue;
	int32 priority;
	int32 priorityLevel;

	virtual void RecalculatePriority();

public:
	virtual void SetNewValue(int32 newValue);
	int32 GetPriority();
	int32 GetPriorityLevel();
	void SetPriorityLevel(int32 level);
};
