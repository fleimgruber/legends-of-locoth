// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "./Enumerations/AIEnumerations.h"
#include "Objective.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UObjective : public UObject
{
	GENERATED_BODY()
public:
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Objective")
		EImportanceEnum Importance;
	
		FORCEINLINE bool operator <(const UObjective& otherObjective) const
		{
			if (Importance < otherObjective.Importance)
			{
				return true;
			}
			return false;
		}
};
