// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Skills/Skill.h"
#include "../Skills/BetterScythe.h"
#include "../Skills/ThreeFieldCropRotation.h"
#include "../Skills/Crystals.h"
#include "../Skills/GoDeeper.h"
#include "../Skills/SteelTools.h"
#include "../Skills/SteelWeapons.h"
#include "../BuildingResearch/BuildingResearch.h"
#include "LocothXmlLoadingController.generated.h"

class UNation;
class ABuilding;

/**
 * This class can load and save the game. Furhtermore, it can
 * load the data for an upgrade. It also can load the slotnames.
 *
 * \class ULocothXmlLoadingController
 */
UCLASS()
class LEGENDSOFLOCOTH_API ULocothXmlLoadingController : public UObject
{
	GENERATED_BODY()

private:
		/**
	 * This Methods initializes a building from the given name.
	 *
	 * \param buildingName is the name of the building
	 * \param place is the location of the building
	 * \param rotator is the rotation of the building
	 * \param world is the world of the game
	 * \param unation is the nation in which the buildings should be saved
	 * \param scale is the scale of the building
	 * \param level is the level of the building
	 *
	 * \return The created building
	 */
	static ABuilding* GetBuilding(FString buildingName, FVector place, FRotator rotator, UWorld *world, UNation *unation, FVector scale, uint8 level);

	/**
	* This Methods initializes the Skill according to the given name.
	*
	* \param skillName is the name of the skill
	*
	* \return The created Skill
	*/
	static USkill* GetSkill(FString skillName);


public:

	/**
	 * This method load all buildings, persons and data
	 * and displays it on the world.
	 *
	 * \param slotName is the name of the game-slotname
	 * \param world is the world of the game
	 * \param playerNation is the nation of the player
	 */
	static void Load(FString slotName, UWorld *world, UNation *playerNation);

	/**
	 * This method saves all buildings, persons and data
	 * from the game.
	 *
	 * \param slotName is the name of the game-slotname
	 * \param world is the world of the game
	 * \param playerNation is the nation of the player
	 *
	 * \return 
	 */
	static bool Save(FString slotName, UWorld *world, UNation *playerNation);

	/**
	 * This method returns the rocks, wood, gold
	 * you need 
	 * 
	 * \param rocks is the return variable for rocks
	 * \param wood is the return variable for wood
	 * \param gold is the return variable for gold
	 * \param level is the requested level
	 * \param id is the name of the building
	 */
	static void GetUpgradeInfo(int32& rocks, int32& wood, int32& gold, uint8 level, FString id);

	/**
	 * This mehtod fetches all Slotnames from the directory.
	 *
	 * \return All Slotname
	 */
	UFUNCTION(BlueprintCallable, Category = "SlotNames")
	static TArray<FString> GetSlotnames();
	
};
