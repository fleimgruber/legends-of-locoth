// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "LocothXmlLoadingController.h"
#include "XmlParser.h"
#include "LegendsOfLocoth/LocothGameState.h"
#include "../Buildings/Building.h"
#include "../Buildings/Barracks.h"
#include "../Buildings/Building.h"
#include "../Buildings/Farm.h"
#include "../Buildings/University.h"
#include "../Buildings/GoldMine.h"
#include "../Buildings/Harbour.h"
#include "../Buildings/House.h"
#include "../Buildings/MarketPlace.h"
#include "../Buildings/StoneQuarry.h"
#include "../Buildings/WoodCutter.h"
#include "../Buildings/TownCenter.h"
#include <string>
#include "LegendsOfLocoth/Controller/NationAI.h"
#include "../LocothGameState.h"
#include "../BuildingResearch/BuildingResearch.h"
#include <LegendsOfLocoth/HelperClasses/UpgradeInfoTmp.h>
#include <LegendsOfLocoth/HelperClasses/ObjectHolder.h>


ABuilding* ULocothXmlLoadingController::GetBuilding(FString buildingName, FVector place
	, FRotator rotator, UWorld *world, UNation *unation, FVector scale, uint8 level)
{
	ABuilding *ret = nullptr;
	
	if (buildingName.Equals(TEXT("Barracks")))
		ret=world->SpawnActor<ABarracks>(place,rotator);
	else if (buildingName.Equals(TEXT("Farm")))
		ret=world->SpawnActor<AFarm>(place,rotator);
	else if (buildingName.Equals(TEXT("University")))
		ret=world->SpawnActor<AUniversity>(place,rotator);
	else if (buildingName.Equals(TEXT("Gold Mine")))
		ret=world->SpawnActor<AGoldMine>(place,rotator);
	else if (buildingName.Equals(TEXT("Harbour")))
		ret=world->SpawnActor<AHarbour>(place,rotator);
	else if (buildingName.Equals(TEXT("House")))
		ret=world->SpawnActor<AHouse>(place,rotator);
	else if (buildingName.Equals(TEXT("Market Place")))
		ret=world->SpawnActor<AMarketPlace>(place,rotator);
	else if (buildingName.Equals(TEXT("Stone Quarry")))
		ret=world->SpawnActor<AStoneQuarry>(place,rotator);
	else if (buildingName.Equals(TEXT("Town Center")))
		ret=world->SpawnActor<ATownCenter>(place,rotator);
	else if (buildingName.Equals(TEXT("Wood Cutter")))
		ret = world->SpawnActor<AWoodCutter>(place, rotator);
	else
		throw 20; //unknown building

	ret->nation = unation;
	
	ret->SetLevel(level);
	
	ret->SetActorScale3D(scale);
	ret->OnPlaced();
	

	return ret;
}

USkill* ULocothXmlLoadingController::GetSkill(FString skillName)
{
	USkill *ret = nullptr;
	if (skillName.Equals(TEXT("Better Scythe")))
		ret = NewObject<UBetterScythe>();
	else if (skillName.Equals(TEXT("Crystals")))
		ret = NewObject<UCrystals>();
	else if (skillName.Equals(TEXT("We Need To Go Deeper")))
		ret = NewObject<UGoDeeper>();
	else if (skillName.Equals(TEXT("Steel Tools")))
		ret = NewObject<USteelTools>();
	else if (skillName.Equals(TEXT("Steel Weapons")))
		ret = NewObject<USteelWeapons>();
	else if (skillName.Equals(TEXT("Three Field Crop Rotation")))
		ret = NewObject<UThreeFieldCropRotation>();
	else
		throw 20;
	return ret;
}

void ULocothXmlLoadingController::Load(FString slotName, UWorld *world, UNation *playerNation)
{
	
	FString loadPath = FPaths::GameDir().Append(TEXT("/save/")).Append(slotName).Append(TEXT(".xml"));

	FXmlFile file (loadPath);
	auto pRoot = file.GetRootNode();
	
	ALocothGameState *gameState = world->GetGameState<ALocothGameState>();

	for (auto subNode : pRoot->GetChildrenNodes())
	{
		

		int32 gold = FCString::Atoi(*(subNode->FindChildNode(TEXT("gold"))->GetContent()));
		int32 rocks = FCString::Atoi(*(subNode->FindChildNode(TEXT("rocks"))->GetContent()));
		int32 wood = FCString::Atoi(*(subNode->FindChildNode(TEXT("wood"))->GetContent()));
		int32 food = FCString::Atoi(*(subNode->FindChildNode(TEXT("food"))->GetContent()));
		FString isOwnNation = subNode->FindChildNode(TEXT("isOwnNation"))->GetContent();
		int32 nationId = FCString::Atoi(*subNode->FindChildNode(TEXT("nationId"))->GetContent());
		
		UNation *nation=playerNation;
		if (!isOwnNation.Equals(TEXT("true")))
		{
			int32 opponentId = FCString::Atoi(*isOwnNation);
			nation=Cast<ANationAI>(gameState->GetOpponent(opponentId)->GetController())->nation;
		}

		nation->nationId = nationId;


		TArray<ABuilding *> buildings;

		for (auto buildingNode : subNode->FindChildNode(TEXT("buildings"))->GetChildrenNodes())
		{
			FString buildingName = buildingNode->FindChildNode(TEXT("name"))->GetContent();

			float x = FCString::Atof(*buildingNode->FindChildNode(TEXT("X"))->GetContent());
			float y = FCString::Atof(*buildingNode->FindChildNode(TEXT("Y"))->GetContent());
			float z = FCString::Atof(*buildingNode->FindChildNode(TEXT("Z"))->GetContent());

			float yaw = FCString::Atof(*buildingNode->FindChildNode(TEXT("Yaw"))->GetContent());
			float pitch = FCString::Atof(*buildingNode->FindChildNode(TEXT("Pitch"))->GetContent());
			float roll = FCString::Atof(*buildingNode->FindChildNode(TEXT("Roll"))->GetContent());

			float scaleX = FCString::Atof(*buildingNode->FindChildNode(TEXT("scaleX"))->GetContent());
			float scaleY = FCString::Atof(*buildingNode->FindChildNode(TEXT("scaleY"))->GetContent());
			float scaleZ = FCString::Atof(*buildingNode->FindChildNode(TEXT("scaleZ"))->GetContent());

			int32 health = FCString::Atof(*buildingNode->FindChildNode(TEXT("health"))->GetContent());
			
			int32 level = FCString::Atoi(*buildingNode->FindChildNode(TEXT("level"))->GetContent());
			
			int32 workerCount = FCString::Atoi(*buildingNode->FindChildNode(TEXT("workerCount"))->GetContent());;

			FVector vector(x,y,z);
			FRotator rotator(pitch, yaw, roll);
			FVector scale(scaleX, scaleY, scaleZ);

			ABuilding* building = GetBuilding(buildingName, vector, rotator, world, nation, scale, level);
			building->SetWorkerCount(workerCount);
			building->SetHealth(health);
			buildings.Add(building);
		}

		for (auto personNode : subNode->FindChildNode(TEXT("persons"))->GetChildrenNodes())
		{

			float x = FCString::Atof(*personNode->FindChildNode(TEXT("X"))->GetContent());
			float y = FCString::Atof(*personNode->FindChildNode(TEXT("Y"))->GetContent());
			float z = FCString::Atof(*personNode->FindChildNode(TEXT("Z"))->GetContent());

			float yaw = FCString::Atof(*personNode->FindChildNode(TEXT("Yaw"))->GetContent());
			float pitch = FCString::Atof(*personNode->FindChildNode(TEXT("Pitch"))->GetContent());
			float roll = FCString::Atof(*personNode->FindChildNode(TEXT("Roll"))->GetContent());

			float scaleX = FCString::Atof(*personNode->FindChildNode(TEXT("scaleX"))->GetContent());
			float scaleY = FCString::Atof(*personNode->FindChildNode(TEXT("scaleY"))->GetContent());
			float scaleZ = FCString::Atof(*personNode->FindChildNode(TEXT("scaleZ"))->GetContent());
			
			int32 health = FCString::Atoi(*personNode->FindChildNode(TEXT("health"))->GetContent());

			FVector location(x, y, z);
			FRotator rotator(pitch, yaw, roll);
			FVector scale(scaleX, scaleY, scaleZ);
			
			if (personNode->FindChildNode(TEXT("isSoldier"))->GetContent().ToBool())
				nation->SpawnSoldier(location, rotator, scale, health);
			else
				nation->SpawnVillager(location, rotator, scale, health);
		}
		
		TArray<USkill*> alreadyDiscovered;
		for (auto researchNode : subNode->FindChildNode(TEXT("research"))->GetChildrenNodes())
		{
			FString buildingName = *researchNode->FindChildNode(TEXT("name"))->GetContent();
			int32 efficiency = FCString::Atoi(*researchNode->FindChildNode(TEXT("efficiency"))->GetContent());


			UBuildingResearch* res = UBuildingResearch::GetInstance(buildingName, nationId);
			res->SetEfficiency(efficiency);

			for (auto researchSkillNode : researchNode->FindChildNode(TEXT("skills"))->GetChildrenNodes())
			{
				USkill* skill = GetSkill(researchSkillNode->GetContent());
				res->AddSkill(skill);
				alreadyDiscovered.Add(skill);
			}
		}

		nation->gold = gold;
		nation->rocks = rocks;
		nation->wood = wood;
		nation->food = food;

		for (ABuilding* building : buildings)
		{
			AUniversity* uni = Cast<AUniversity>(building);
			if (uni)
			{
				uni->LoadToResearchList(alreadyDiscovered);
				uni->ShuffleSkills();
			}
			nation->AddBuilding(building);
		}
	}

	//FString cont = subNode->GetContent();
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, cont);*/

}

bool ULocothXmlLoadingController::Save(FString slotName, UWorld *world, UNation *playerNation)
{
	FString mainPath = FPaths::GameDir().Append(TEXT("/save/"));


	FXmlFile file(FPaths::GameDir().Append(TEXT("/templates/template.xml")));

	auto rootNode = file.GetRootNode();

	int cityCount = 0;

	TArray<UNation *> nations;

	nations.Add(playerNation);

	
	TArray<AOpponent*> opponents = static_cast<ALocothGameState*>(world->GetGameState())->GetOpponents();

	for (int i = 0; i < opponents.Num(); i++)
	{
		auto nationai = static_cast<ANationAI*>(opponents[i]->GetController());

		if (nationai && nationai->nation)
		{
			nations.Add(nationai->nation);
		}
	}


	for (int i = 0; i < nations.Num(); i++)
	{
		UNation *nation = nations[i];

		rootNode->AppendChildNode(TEXT("nation"), TEXT(""));
		auto nationNode = rootNode->GetChildrenNodes()[i];

		if (i == 0)
		{
			nationNode->AppendChildNode(TEXT("isOwnNation"), TEXT("true"));
		} else
		{
			nationNode->AppendChildNode(TEXT("isOwnNation"), FString::FromInt(i-1));
		}

		nationNode->AppendChildNode(TEXT("nationId"), FString::FromInt(nation->nationId));

		nationNode->AppendChildNode(TEXT("food"), FString::FromInt(nation->GetFood()));
		nationNode->AppendChildNode(TEXT("wood"), FString::FromInt(nation->GetWood()));
		nationNode->AppendChildNode(TEXT("rocks"), FString::FromInt(nation->GetRocks()));
		nationNode->AppendChildNode(TEXT("gold"), FString::FromInt(nation->GetGold()));

		nationNode->AppendChildNode(TEXT("buildings"), TEXT(""));
		auto buildingsNode = nationNode->FindChildNode(TEXT("buildings"));

		for (int j = 0; j < nation->buildings.Num(); j++)
		{
			buildingsNode->AppendChildNode(TEXT("building"), TEXT(""));
			FXmlNode* buildingNode = buildingsNode->GetChildrenNodes()[j];

			auto building = nation->buildings[j];
			auto rotation = building->GetActorRotation();

			auto scale = building->GetActorScale();

			buildingNode->AppendChildNode(TEXT("name"), building->GetName());
			buildingNode->AppendChildNode(TEXT("X"), FString::SanitizeFloat(building->GetActorLocation().X));
			buildingNode->AppendChildNode(TEXT("Y"), FString::SanitizeFloat(building->GetActorLocation().Y));
			buildingNode->AppendChildNode(TEXT("Z"), FString::SanitizeFloat(building->GetActorLocation().Z));

			buildingNode->AppendChildNode(TEXT("Yaw"), FString::SanitizeFloat(rotation.Yaw));
			buildingNode->AppendChildNode(TEXT("Pitch"), FString::SanitizeFloat(rotation.Pitch));
			buildingNode->AppendChildNode(TEXT("Roll"), FString::SanitizeFloat(rotation.Roll));

			buildingNode->AppendChildNode(TEXT("scaleX"), FString::SanitizeFloat(scale.X));
			buildingNode->AppendChildNode(TEXT("scaleY"), FString::SanitizeFloat(scale.Y));
			buildingNode->AppendChildNode(TEXT("scaleZ"), FString::SanitizeFloat(scale.Z));

			buildingNode->AppendChildNode(TEXT("level"), FString::FromInt(building->GetLevel()));

			buildingNode->AppendChildNode(TEXT("health"), FString::FromInt(building->GetHealth()));

			buildingNode->AppendChildNode(TEXT("workerCount"), FString::FromInt(building->GetWorkerCount()));
		}

		nationNode->AppendChildNode(TEXT("persons"), TEXT(""));
		auto personsNode = nationNode->FindChildNode(TEXT("persons"));


		int personCount = 0;
		for (int j = 0; j < nation->persons.Num(); j++)
		{
			APerson* person = nation->persons[j];

			personsNode->AppendChildNode(TEXT("person"), TEXT(""));
			FXmlNode* personNode = personsNode->GetChildrenNodes()[personCount];

			auto rotation = person->GetActorRotation();
			auto scale = person->GetActorScale();
			auto location = person->GetActorLocation();

			personNode->AppendChildNode(TEXT("X"), FString::SanitizeFloat(location.X));
			personNode->AppendChildNode(TEXT("Y"), FString::SanitizeFloat(location.Y));
			personNode->AppendChildNode(TEXT("Z"), FString::SanitizeFloat(location.Z));

			personNode->AppendChildNode(TEXT("Yaw"), FString::SanitizeFloat(rotation.Yaw));
			personNode->AppendChildNode(TEXT("Pitch"), FString::SanitizeFloat(rotation.Pitch));
			personNode->AppendChildNode(TEXT("Roll"), FString::SanitizeFloat(rotation.Roll));

			personNode->AppendChildNode(TEXT("scaleX"), FString::SanitizeFloat(scale.X));
			personNode->AppendChildNode(TEXT("scaleY"), FString::SanitizeFloat(scale.Y));
			personNode->AppendChildNode(TEXT("scaleZ"), FString::SanitizeFloat(scale.Z));
			
			personNode->AppendChildNode(TEXT("isSoldier"), Cast<ASoldier>(person)?"True":"False");
			
			personNode->AppendChildNode(TEXT("health"), FString::FromInt(person->health));
			
			personCount++;
		}



		nationNode->AppendChildNode(TEXT("research"), TEXT(""));
		auto researchNode = nationNode->FindChildNode(TEXT("research"));

		auto researchList = UBuildingResearch::GetAllResearch(nation->nationId);
		int k=0;
		for (auto &res : researchList)
		{
			researchNode->AppendChildNode(TEXT("buildingRes"), TEXT(""));
			FXmlNode* buildingResNode = researchNode->GetChildrenNodes()[k];

			buildingResNode->AppendChildNode(TEXT("name"), res->name);
			buildingResNode->AppendChildNode(TEXT("efficiency"), FString::FromInt(res->efficiency));
			buildingResNode->AppendChildNode(TEXT("skills"), TEXT(""));

			FXmlNode* skillNode = buildingResNode->FindChildNode(TEXT("skills"));
			for (int l = 0; l < res->GetSkills().Num(); l++)
			{
				skillNode->AppendChildNode(TEXT("skillname"), res->GetSkills()[l]->GetName());
			}
			k++;
		}
	}

	FString savePath = FPaths::GameDir().Append(TEXT("/save/")).Append(slotName).Append(TEXT(".xml"));

	return file.Save(savePath);
}


void ULocothXmlLoadingController::GetUpgradeInfo(int32& rocks, int32& wood, int32& gold, uint8 level, FString id)
{
	static TMap<FString, UObjectHolder*> map;

	if (map.Num() == 0)
	{
		FXmlFile file(FPaths::GameDir().Append(TEXT("/templates/canafford.xml")));

		auto rootNode = file.GetRootNode();		

		for (auto buildingNode : rootNode->GetChildrenNodes())
		{
			FString buildingName = buildingNode->GetTag();
			auto holder = NewObject<UObjectHolder>();
			holder->SetFlags(RF_RootSet);

			for (auto levelNode : buildingNode->GetChildrenNodes())
			{
				int ilevel = FCString::Atoi(*levelNode->GetTag().Replace(TEXT("level"), TEXT("")));

				int irocks = FCString::Atoi(*(levelNode->FindChildNode(TEXT("rocks"))->GetContent()));
				int iwood = FCString::Atoi(*(levelNode->FindChildNode(TEXT("wood"))->GetContent()));
				int igold = FCString::Atoi(*(levelNode->FindChildNode(TEXT("gold"))->GetContent()));

				auto info = NewObject<UUpgradeInfoTmp>();
				info->SetFlags(RF_RootSet);

				info->rocks = irocks;
				info->wood = iwood;
				info->gold = igold;

				holder->upgradeInfos.Add(info);
			}

			map.Add(buildingName, holder);
		}
	}
	
	FString search = id.Replace(TEXT(" "), TEXT(""));

	auto holderNode = map[search];

	if (holderNode->upgradeInfos.Num() >= level)
	{
		rocks = holderNode->upgradeInfos[level - 1]->rocks;
		wood = holderNode->upgradeInfos[level - 1]->wood;
		gold = holderNode->upgradeInfos[level - 1]->gold;
	} 
	else
	{
		rocks = 0;
		wood = 0;
		gold = 0;
	}
	
}

TArray<FString> ULocothXmlLoadingController::GetSlotnames()
{
	TArray<FString> res;
	IFileManager& FileManager = IFileManager::Get();
	
	FString path = FPaths::GameDir().Append(TEXT("/save/*.xml"));

	FileManager.FindFiles(res, *path, true, false);

	for (int i = 0; i < res.Num(); i++)
	{
		res[i] = res[i].Mid(0, res[i].Len() - 4);
	}

	return res;
}