#include "LegendsOfLocoth.h"
#include "MenuGameMode.h"


FString AMenuGameMode::CreateNewGame()
{
	FString newGameTemplate = FPaths::GameDir().Append("/templates/newgame.xml");
	FString content;
	FFileHelper::LoadFileToString(content, *newGameTemplate);
	FString slotName = FDateTime::Now().ToString();
	FFileHelper::SaveStringToFile(content, *FPaths::GameDir().Append("/save/").Append(slotName).Append(".xml"));
	return slotName;
}

void AMenuGameMode::DeleteGame(FString name)
{
	FString file = FPaths::GameDir().Append("/save/").Append(name).Append(".xml");
	FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*file);
}