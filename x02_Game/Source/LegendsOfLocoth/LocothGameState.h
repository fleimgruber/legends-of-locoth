#pragma once

#include "GameFramework/GameState.h"
#include "Pawns/Opponent.h"
#include "LocothGameState.generated.h"

/**
* In unreal engine every game has a game state, which should be used to.. well, represent the games current state
*/
UCLASS()
class LEGENDSOFLOCOTH_API ALocothGameState : public AGameState
{
	GENERATED_BODY()
    
private:
    TArray<AOpponent*> opponents;
    float lastDay;
    uint32 now;
	
public:
	TSubclassOf<class APerson> VillagerBP, SoldierBP;
	
    ALocothGameState(const FObjectInitializer& ObjectInitializer);
	
	/**
	* Called when the games begins
	*/
    virtual void BeginPlay() override;
	
	/**
	* Called every frame
	*
	* \param delatT, time since last frame
	*/
    virtual void Tick(float deltaT) override;
	
	/**
	* the games states keeps the in-game time
	* this method returns true, if sinece the given time one day has passed
	* 
	* \param myTime - time to check against
	*
	* \return true, in case it is a new day
	*/
    bool IsNewDay(uint32 &myTime);

    UFUNCTION(BlueprintCallable, Category = Opponents)
    AOpponent *GetOpponent(int32 idx);
	
	/*
	* Returns an opponent that has the matching name.
	* \return all opponents
	*/
    UFUNCTION(BlueprintCallable, Category = Opponents)
    TArray<AOpponent*> GetOpponents();

	/*
	* Returns an opponent that has the matching name.
	* \param name of the opponent
	* \return opponent
	*/
	UFUNCTION(BlueprintCallable, Category = Opponents)
	AOpponent* GetOpponentByName(FString name);

	UFUNCTION(BlueprintCallable, Category = SaveGame)
	static FString &GetSlotName();
	
	UFUNCTION(BlueprintCallable, Category = SaveGame)
	static void SetSlotName(FString value);
    
};
