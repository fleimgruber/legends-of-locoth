#pragma once

#include "Object.h"
#include "NotificationData.generated.h"

/**
* Used to exchange information between the notification center and notification data
*/
UCLASS(BlueprintType)
class LEGENDSOFLOCOTH_API UNotificationData : public UObject
{
	GENERATED_BODY()
	
protected:
	FString title, subtitle;

public:
	UFUNCTION(BlueprintCallable, Category = Data)
	FString GetTitle() const;
    
	UFUNCTION(BlueprintCallable, Category = Data)
	FString GetSubtitle() const;
	

	UFUNCTION(BlueprintCallable, Category = Data)
	void SetTitle(FString _title);
    
	UFUNCTION(BlueprintCallable, Category = Data)
	void SetSubtitle(FString _subtitle);
};
