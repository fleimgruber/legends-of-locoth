#include "LegendsOfLocoth.h"
#include "NotificationData.h"


FString UNotificationData::GetTitle() const
{
	return title;
}

FString UNotificationData::GetSubtitle() const
{
	return subtitle;
}

void UNotificationData::SetTitle(FString _title)
{
	title = _title;
}


void UNotificationData::SetSubtitle(FString _subtitle)
{
	subtitle = _subtitle;
}