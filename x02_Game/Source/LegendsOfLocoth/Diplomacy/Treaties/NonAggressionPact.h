// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Object.h"
#include "Treaty.h"
#include "NonAggressionPact.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LEGENDSOFLOCOTH_API UNonAggressionPact : public UTreaty
{
	GENERATED_BODY()

public:
	UNonAggressionPact(const FObjectInitializer& ObjectInitialize);

	UFUNCTION(BlueprintCallable, Category = Treaty)
	static FString GetNameStatic() { return "Non Aggression Pact"; };

	UFUNCTION(BlueprintCallable, Category = Treaty)
	static FString GetDescriptionStatic() { return "This Treaty states that two parties cannot go to war as long as it exists."; };

	UFUNCTION(BlueprintCallable, Category = Treaty)
	FString GetName();
	
};