// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Treaty.h"

void UTreaty::AcceptTreaty(){
	status = "accepted";
	SendNotification("Treaty accepted", "");
	this->sourceNation->AddTreaty(this);
	this->receiveNation->AddTreaty(this);
	//TODO more implementation -> what exactly happens?
}

void UTreaty::DenyTreaty(){
	SendNotification("Treaty denied", "");
}

void UTreaty::SendNotification(FString title, FString content)
{
	ALocothPlayerController *playerController = Cast<ALocothPlayerController>(sourceNation->world->GetFirstPlayerController());
	if (playerController->nation == sourceNation || playerController->nation == receiveNation)
	{
		auto n = NewObject<UNotificationData>();
		n->SetTitle(title);
		n->SetSubtitle(content);
		playerController->ShowNotification(n);
	}
}