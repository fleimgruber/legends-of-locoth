#pragma once

#include "Object.h"
#include "../../Controller/Nation.h"
#include "Treaty.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class LEGENDSOFLOCOTH_API UTreaty : public UObject
{
	GENERATED_BODY()

		void SendNotification(FString title, FString content);

	public:
		UPROPERTY(BlueprintReadWrite, Category=Treaty)
		FString name;
		UPROPERTY(BlueprintReadWrite, Category=Treaty)
		FString description;
		UPROPERTY(BlueprintReadWrite, Category = Treaty)
		FString status;
		UPROPERTY(BlueprintReadWrite, Category = Treaty)
		UNation *sourceNation;
		UPROPERTY(BlueprintReadWrite, Category = Treaty)
		UNation *receiveNation;

		/**
		* When a treaty is accepted this function is called.
		*/
		UFUNCTION(BlueprintCallable, Category = Treaty)
		void AcceptTreaty();

		/**
		* When a treaty is denied this function is called.
		*/
		UFUNCTION(BlueprintCallable, Category = Treaty)
		void DenyTreaty();
};