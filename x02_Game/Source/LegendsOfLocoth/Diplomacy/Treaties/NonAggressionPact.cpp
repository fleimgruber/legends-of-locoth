// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "NonAggressionPact.h"

UNonAggressionPact::UNonAggressionPact(const FObjectInitializer& ObjectInitialize) : UTreaty(ObjectInitialize)
{
	name = TEXT("Non Agression Pact");
}

FString UNonAggressionPact::GetName(){
	return name;
}