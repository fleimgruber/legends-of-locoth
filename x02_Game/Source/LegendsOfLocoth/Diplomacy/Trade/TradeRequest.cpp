// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "../../Notifications/NotificationData.h"
#include "TradeRequest.h"



void UTradeRequest::SendNotification(FString title, FString content)
{
	ALocothPlayerController *playerController = Cast<ALocothPlayerController>(from->world->GetFirstPlayerController());
	if (playerController->nation == from||playerController->nation == to)
	{
		auto n=NewObject<UNotificationData>();
		n->SetTitle(title);
		n->SetSubtitle(content);
		playerController->ShowNotification(n);
	}
}

void UTradeRequest::Accept()
{
	from->gold += goldTake-goldGive;
	to->gold += -(goldTake-goldGive);
	from->wood += woodTake-woodGive;
	to->wood += -(woodTake-woodGive);
	from->rocks += rocksTake-rocksGive;
	to->rocks += -(rocksTake-rocksGive);
	from->food += foodTake-foodGive;
	to->food += -(foodTake-foodGive);
	SendNotification("Request Accepted","");
}

void UTradeRequest::Deny()
{
	SendNotification("Request Denied","");
}
