// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../../Controller/Nation.h"
#include "TradeRequest.generated.h"

/**
 * represents someone (from) sneind a request to someone else (to)
 * fields for all resources that get traded
 */
UCLASS(BlueprintType)
class LEGENDSOFLOCOTH_API UTradeRequest : public UObject
{
	GENERATED_BODY()
	
	void SendNotification(FString title, FString content);
	
public:
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 goldGive;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 goldTake;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 woodGive;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 woodTake;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 rocksGive;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 rocksTake;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 foodGive;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	int32 foodTake;
	
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	UNation *from;
	UPROPERTY(BlueprintReadWrite, Category=Trade)
	UNation *to;
	
	/*
	* Accepts the given trade + trades the money
	*/
	UFUNCTION(BlueprintCallable, Category=Trade)
	void Accept();
	
	/*
	 * Denies the trade
	*/
	UFUNCTION(BlueprintCallable, Category=Trade)
	void Deny();
	
};
