#pragma once

#include "GameFramework/GameMode.h"
#include "MenuGameMode.generated.h"

/**
 * the menus game mode
 * needed because some functions are not availble in blueprints
 */
UCLASS()
class LEGENDSOFLOCOTH_API AMenuGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	
	/**
	* Creates a new game and saves it to the disk
	*
	* \return the name of the game created
	*/
	UFUNCTION(BlueprintCallable, Category=Menu)
	FString CreateNewGame();
	
	/**
	* Deletes the name with the given name
	*
	* \param name - game to delete
	*/
	UFUNCTION(BlueprintCallable, Category=Menu)
	void DeleteGame(FString name);
	
};
