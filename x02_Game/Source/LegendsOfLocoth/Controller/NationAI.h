#pragma once

#include "AIController.h"
#include "Nation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "../Buildings/Building.h"
#include "../Pawns/Person.h"
#include "../AIHelper/PopulationNeedsMonitor.h"
#include "../AIHelper/ResourceNeedsMonitor.h"
#include "../AIHelper/Objective.h"
#include "../AIHelper/Enumerations/AIEnumerations.h"
#include "NationCallback.h"
#include "NationAI.generated.h"


UCLASS()
class LEGENDSOFLOCOTH_API ANationAI : public AAIController, public INationCallback
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
	int32 agressiveness;

	UPROPERTY()
	TArray<UObjective*> objectives;

	UPROPERTY()
	TSubclassOf<ABuilding> nextBuilding;
	
	bool buyVillager;
	int32 buildingState;

	UPROPERTY()
	uint32 myTime;

	UPROPERTY()
	UPopulationNeedsMonitor* populationMonitor;

	UPROPERTY()
	UResourceNeedsMonitor* foodMonitor;

	UPROPERTY()
	UResourceNeedsMonitor* stoneMonitor;

	UPROPERTY()
	UResourceNeedsMonitor* woodMonitor;

	UPROPERTY()
	UResourceNeedsMonitor* goldMonitor;

	UFUNCTION()
	void InitiateObjectives();

	UFUNCTION()
	void InitiateResourceObjectives();

	UFUNCTION()
	void SortObjectives();

	UFUNCTION()
	bool CanAffordTrade(UTradeRequest* tradeReq);

	TArray<UTreaty*> treaties;

	TArray<UTradeRequest*> tradeRequests;

	UPROPERTY()
	TArray<int32> deltaX;
	UPROPERTY()
	TArray<int32> deltaY;
	int32 ptr;
	
public:
	UFUNCTION(BlueprintCallable, Category = Callback)
	void ReceiveTreatyRequest(UTreaty *treaty) override;
	
	/*
	* Called when someone wants to trade with this AI
	*/
	UFUNCTION(BlueprintCallable, Category=Callback)
	void ReceiveTradeRequest(UTradeRequest *trade) override;

    UPROPERTY(transient)
    UBlackboardComponent *BlackboardComp;
    
    UPROPERTY(transient)
    UBehaviorTreeComponent *BehaviorComp;
    
    UPROPERTY(BlueprintReadOnly)
    UNation *nation;
    
    ANationAI(const FObjectInitializer& ObjectInitializer);
	
	/**
	 *  Called every time the games begins (created because we can't rely on the constructor, unreal engine reused the objects for differents games)
	 */
	void Init();
	
	/**
	 *  Called by the engine once a pawn is assigned this controller, usually the beginning of the game
	 *
	 * \param pawn - the possessed pawn
	 */
    virtual void Possess(APawn *pawn) override;

	/**
	 *  lets the AI think about the thing it should spend money on next
	 */
	UFUNCTION(BlueprintCallable, Category=AI)
	void ThinkAboutBuying();
	
	/**
	 *  tells the AI to build the next building it has thought of (if there are enough resouces)
	 */
	UFUNCTION(BlueprintCallable, Category=AI)
	ABuilding* BuildBuilding();
	
	/**
	 *  builds a new villagers, if there are enough resources
	 */
	UFUNCTION(BlueprintCallable, Category=AI)
	void BuildVillagers();

	UFUNCTION(BlueprintCallable, Category=AI)
	void UpdateInfoLibrary();

	UFUNCTION(BlueprintCallable, Category = AI)
	void ActUponObjectives();

	UFUNCTION(BlueprintCallable, Category = AI)
	void AITick();

	UFUNCTION(BlueprintCallable, Category=AI)
	void ReconsiderObjectives();

	UFUNCTION(BlueprintCallable, Category = AI)
	void ActUponTreatiesAndTrade();

	UFUNCTION(BlueprintCallable, Category = AI)
	void ActUponEconomicObjective(UEconomicObjective* ecoObjective);
};
