// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Diplomacy/Treaties/Treaty.h"
#include "../Diplomacy/Trade/TradeRequest.h"
#include "NationCallback.generated.h"

UINTERFACE(Blueprintable)
class UNationCallback : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class INationCallback 
{
	GENERATED_IINTERFACE_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = Callback)
	virtual void ReceiveTreatyRequest(UTreaty *treaty);

	UFUNCTION(BlueprintCallable, Category=Callback)
	virtual void ReceiveTradeRequest(UTradeRequest *trade);

};
