#pragma once

#include "Object.h"
#include "LegendsOfLocoth/Pawns/Person.h"
#include "LegendsOfLocoth/Pawns/Soldier.h"
#include "Nation.generated.h"


class ABuilding;
class UTreaty;
class INationCallback;

UCLASS()
class LEGENDSOFLOCOTH_API UNation : public UObject
{
	GENERATED_BODY()
	
public:
	INationCallback *callback;

	UPROPERTY()
	UWorld *world;

	UPROPERTY(BlueprintReadOnly)
	int32 nationId;
		
	UNation(const FObjectInitializer& ObjectInitializer);
	
	int32 gold, rocks, wood, food, maxpopulation;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<ABuilding*> buildings;

	UPROPERTY(BlueprintReadOnly)
	TArray<UTreaty*> treaties;

	//UPROPERTY()
	TArray<APerson*> persons; //only none-working persons

	/**
	* Gets the nations buildings
	*
	* \return buildings
	*/
	UFUNCTION(BlueprintCallable, Category = Buildings)
	TArray<ABuilding*> GetBuildings() const;

	/**
	 * Gets the nations current amount of gold
	 *
	 * \return gold
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	int32 GetGold() const;
	
	/**
	 * Gets the nations current amount of rocks
	 *
	 * \return rocks
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	int32 GetRocks() const;
	
	/**
	 * Gets the nations current amount of wood
	 *
	 * \return wood
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	int32 GetWood() const;
	
	/**
	 * Gets the nations current amount of food
	 *
	 * \return food
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	int32 GetFood() const;

	/**
	 * Gets the nations current population
	 *
	 * \return population
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	int32 GetPopulation() const;

	/**
	 * Gets the nations current maximum population (depending on the number of houses built)
	 *
	 * \return max population
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	int32 GetMaxPopulation() const;
	
	/**
	* Sets the amount of gold
	*
	* \param gold
	*/
	UFUNCTION(BlueprintCallable, Category = Stats)
	void SetGold(int32 gold);

	/**
	 * Sets the amount of rocks
	 *
	 * \param rocks
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	void SetRocks(int32 rocks);

	/**
	 * Sets the amount of wood
	 *
	 * \param wood
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	void SetWood(int32 wood);

	/**
	 * Sets the amount of food
	 *
	 * \param food
	 */
	UFUNCTION(BlueprintCallable, Category = Stats)
	void SetFood(int32 food);
	
	/**
	 * Adds a building to the city, should be called after the user bought one
	 *
	 * \param building
	 */
	UFUNCTION(BlueprintCallable, Category = Buildings)
	void AddBuilding(ABuilding *building);

	/**
	 * Tells the nation that there's a new person
	 *
	 * \param person
	 */
	UFUNCTION(BlueprintCallable, Category = Persons)
	void AddPerson(APerson* person);

	/**
	* Tells the nation that there's a new treaty
	*
	* \param treaty
	*/
	UFUNCTION(BlueprintCallable, Category = Treaties)
	void AddTreaty(UTreaty* treaty);
	
	/**
	 * Called each day (in-game time)
	 */
	void NewDay();

	/**
	 * Spawns a villager with the given parameters and adds him to this nation
	 *
	 * \param location to spawn
	 * \param fRotator - rotation
	 * \param scale
	 *
	 * \return the new person
	 */
	UFUNCTION(BlueprintCallable, Category = Villager)
	APerson *SpawnVillager(FVector location, FRotator fRotator, FVector scale, int32 health=100);
	
	/**
	 * Spawns a soldier with the given parameters and adds him to this nation
	 *
	 * \param location to spawn
	 * \param fRotator - rotation
	 * \param scale
	 *
	 * \return the new soldier
	 */
	UFUNCTION(BlueprintCallable, Category = Villager)
	ASoldier *SpawnSoldier(FVector location, FRotator fRotator, FVector scale, int32 health=100);

};
