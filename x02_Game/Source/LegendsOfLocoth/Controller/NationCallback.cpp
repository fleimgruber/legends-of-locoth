// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "NationCallback.h"

UNationCallback::UNationCallback(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){

}

void INationCallback::ReceiveTreatyRequest(UTreaty *treaty){
	//implemented in other classes (LocothPlayerController and NationAI)
}

void INationCallback::ReceiveTradeRequest(UTradeRequest *trade) { }
