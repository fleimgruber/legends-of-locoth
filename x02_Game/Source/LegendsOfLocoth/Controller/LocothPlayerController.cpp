#include "LegendsOfLocoth.h"
#include "LocothPlayerController.h"
#include "../LocothGameState.h"
#include "../XmlController/LocothXmlLoadingController.h"

ALocothPlayerController::ALocothPlayerController(const FObjectInitializer& ObjectInitializer) : APlayerController(ObjectInitializer)
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

void  ALocothPlayerController::Init()
{
	nation = NewObject<UNation>();
	nation->callback = this;
	nation->world = GetWorld();
}

void ALocothPlayerController::Tick(float deltaT)
{
	Super::Tick(deltaT);

	auto gameState = GetWorld()->GetGameState<ALocothGameState>();
	if (gameState && gameState->IsNewDay(myTime))
		nation->NewDay();
}

bool ALocothPlayerController::SaveGame(FString slotName)
{
	return ULocothXmlLoadingController::Save(slotName, GetWorld(), this->nation);
}

void ALocothPlayerController::LoadGame(FString slotName)
{
	ULocothXmlLoadingController::Load(slotName, GetWorld(), this->nation);
}

void ALocothPlayerController::ShowNotification_Implementation(UNotificationData *notificationData) { /*implemented in blueprints*/ }

void ALocothPlayerController::AddHealthBar_Implementation(ABuilding *building) { /*implemented in blueprints*/ }

void ALocothPlayerController::SetMousePosition(int32 x, int32 y)
{
	CastChecked<ULocalPlayer>(this->Player)->ViewportClient->Viewport->SetMouse(x, y);
}

void ALocothPlayerController::ShowControllerHUD_Implementation(AActor *selectedActor) { /* implemented in blueprints */ }

void ALocothPlayerController::Buy(int32 gold, int32 wood, int32 rocks)
{
	nation->gold -= gold;
	nation->wood -= wood;
	nation->rocks -= rocks;
}

void ALocothPlayerController::ReceiveTreatyRequest(UTreaty *treaty){
	if (treaty->receiveNation && treaty->sourceNation){
		treaty->status = "pending";
		FString str = treaty->sourceNation->GetName() + " wants to send you a " + treaty->GetName() + "!\n\n Do you want to accept or deny?";
		ShowTreatyDialog(treaty, str);
	}
}


void ALocothPlayerController::ReceiveTradeRequest(UTradeRequest *trade)
{
	FString str = FString::FromInt(trade->goldTake) + " for "+FString::FromInt(trade->goldGive)+"\n";
	str += FString::FromInt(trade->woodTake) + " for "+FString::FromInt(trade->woodGive)+"\n";
	str += FString::FromInt(trade->rocksTake) + " for "+FString::FromInt(trade->rocksGive)+"\n";
	str += FString::FromInt(trade->foodTake) + " for "+FString::FromInt(trade->foodGive);
	ShowTradeDialog(trade, str);
}

void ALocothPlayerController::ShowTradeDialog_Implementation(UTradeRequest *trade, const FString &text)
{
	//implemented in blueprints
}

void ALocothPlayerController::ShowTreatyDialog_Implementation(UTreaty *treaty, const FString &text)
{
	//implemented in blueprints
}

void ALocothPlayerController::ShowAttackForbiddenDialog_Implementation(const FString &text)
{
	//implemented in blueprints
}