#pragma once

#include "GameFramework/PlayerController.h"
#include "../Notifications/NotificationData.h"
#include "Nation.h"
#include "NationCallback.h"
#include "LocothPlayerController.generated.h"

UCLASS()
class LEGENDSOFLOCOTH_API ALocothPlayerController : public APlayerController, public INationCallback
{
	GENERATED_BODY()
    
private:
    uint32 myTime;
	
public:
	/**
	* Called when the opponent sends a treaty to the player
	* \param treaty - the treaty request asked
	*/
	void ReceiveTreatyRequest(UTreaty *treaty) override;
	
	/**
	* Called when the opponent wants to trade with the player
	* \param trade - the trade request asked
	*/
	void ReceiveTradeRequest(UTradeRequest *trade) override;

	/**
	* Show the Accept/Deny dialog for a given or treaty trade, using some info text
	* \param trade
	* \param text
	 */
	UFUNCTION(BlueprintNativeEvent, Category = Trade)
	void ShowTradeDialog(UTradeRequest *trade, const FString &text);

	UFUNCTION(BlueprintNativeEvent, Category = Treaty)
	void ShowTreatyDialog(UTreaty *treaty, const FString &text);

	/**
	* Shows a dialog when attacking another person or building is not allowed due to a pact.
	*/
	UFUNCTION(BlueprintNativeEvent, Category = War)
	void ShowAttackForbiddenDialog(const FString &text);

    UPROPERTY(BlueprintReadOnly)
    UNation *nation;
    
	ALocothPlayerController(const FObjectInitializer& ObjectInitializer);
	
	/**
	*  Called every time the games begins (created because we can't rely on the constructor, unreal engine reused the objects for differents games)
	*/
	void Init();
	
	/**
	* Called every frame
	*/
	virtual void Tick(float deltaT) override;

	/**
	* Saves the game into a given slotname
	*
	* \param slotname to save into
	*
	* \returns true in case of success
	*/
	UFUNCTION(BlueprintCallable, Category = SaveGame)
    bool SaveGame(FString slotName);
	/**
	 * Loads a game into from given slotname
	 *
	 * \param slotname to load from
	 */
	UFUNCTION(BlueprintCallable, Category = SaveGame)
    void LoadGame(FString slotName);

	/**
	 * Sends the given notification to the user
	 *
	 * \param notificationData contains the infos about the notification
	 */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Notifications)
	void ShowNotification(UNotificationData *notificationData);
	
	/**
	* Adds a health bar to the given building
	*/
	UFUNCTION(BlueprintNativeEvent, Category=Building)
	void AddHealthBar(ABuilding *building);
	
	/**
	 * Sets the mouse positon - created because there's no function for that in blueprints
	 *
	 * \param x coordinate
	 * \param y coordinate
	 */
    UFUNCTION(BlueprintCallable, Category = Mouse)
    void SetMousePosition(int32 x, int32 y);
	
	/**
	 * Should be called when an actor is clicked on
	 * This method figures out the corred HUD to display in this case
	 *
	 * \param selectedActor - the actor clicked onto
	 */
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = HUDs)
    void ShowControllerHUD(AActor *selectedActor);

	/**
	* Subtracts the given amount from the nations resources
	*/
	UFUNCTION(BlueprintCallable, Category = Resources)
	void Buy(int32 gold, int32 wood, int32 rocks);
	
};