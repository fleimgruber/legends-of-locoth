#include "LegendsOfLocoth.h"
#include "Nation.h"
#include "../Buildings/Building.h"
#include "../Diplomacy/Treaties/Treaty.h"
#include "NationCallback.h"
#include "../LocothGameState.h"


UNation::UNation(const FObjectInitializer& ObjectInitializer) : UObject(ObjectInitializer)
{
	gold = rocks = wood = food = 0;
	maxpopulation = 10;
	buildings.Empty();
	persons.Empty();
}

void UNation::AddBuilding(ABuilding *building)
{
	building->nation = this;
	buildings.Add(building);
}

void UNation::AddPerson(APerson* person)
{
	person->nation = this;
	persons.Add(person);
}

void UNation::AddTreaty(UTreaty *treaty)
{
	treaties.Add(treaty);
}

int32 UNation::GetGold() const
{
	return gold;
}

int32 UNation::GetRocks() const
{
	return rocks;
}

int32 UNation::GetWood() const
{
	return wood;
}

int32 UNation::GetFood() const
{
	return food;
}

TArray<ABuilding*> UNation::GetBuildings() const
{
	return buildings;
}

int32 UNation::GetPopulation() const
{
	int32 r=persons.Num();
	for (ABuilding *building : buildings)
		r += building->GetWorkerCount();
	return r;
}

int32 UNation::GetMaxPopulation() const
{
	return maxpopulation;
}

void UNation::SetGold(int32 g)
{
	gold = g;
}

void UNation::SetRocks(int32 r)
{
	rocks = r;
}

void UNation::SetWood(int32 w)
{
	wood = w;
}

void UNation::SetFood(int32 f) 
{
	food = f;
}


void UNation::NewDay()
{
	for (ABuilding *building : buildings)
		building->NewDay();
}

APerson *UNation::SpawnVillager(FVector location, FRotator fRotator, FVector scale, int32 health)
{
	ALocothGameState *gameState = world->GetGameState<ALocothGameState>();
	APerson *person = world->SpawnActor<APerson>(gameState->VillagerBP, location, fRotator);
	person->health = health;
	person->SetActorScale3D(scale);
	person->SpawnDefaultController();
	AddPerson(person);
	return person;
}

ASoldier *UNation::SpawnSoldier(FVector location, FRotator fRotator, FVector scale, int32 health)
{
	ALocothGameState *gameState = world->GetGameState<ALocothGameState>();
	ASoldier *person = world->SpawnActor<ASoldier>(gameState->SoldierBP, location, fRotator);
	person->health = health;
	person->SetActorScale3D(scale);
	person->SpawnDefaultController();
	AddPerson(person);
	return person;
}