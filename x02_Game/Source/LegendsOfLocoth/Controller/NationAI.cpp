#include "LegendsOfLocoth.h"
#include "../Pawns/Opponent.h"
#include "../Buildings/Building.h"
#include "NationAI.h"
#include "../Buildings/Building.h"
#include "../Buildings/House.h"
#include "../Buildings/Farm.h"
#include "../Buildings/GoldMine.h"
#include "../Buildings/StoneQuarry.h"
#include "../Buildings/WoodCutter.h"
#include "../Buildings/TownCenter.h"
#include "LocothPlayerController.h"


ANationAI::ANationAI(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackboardComp"));
	BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));
}

void ANationAI::Init()
{
	nation = NewObject<UNation>();
	nation->callback = this;
	nation->world = GetWorld();
	myTime = -1;
	agressiveness = 7;
	UpdateInfoLibrary();
	InitiateResourceObjectives();
	tradeRequests = TArray<UTradeRequest*>();
	treaties = TArray<UTreaty*>();
}

void ANationAI::Possess(APawn *pawn)
{
	Super::Possess(pawn);
	
	AOpponent *opponent = Cast<AOpponent>(pawn);
	if (opponent && opponent->Behavior)
	{
		BlackboardComp->InitializeBlackboard(*opponent->Behavior->BlackboardAsset);
		
		//getkeyid: (uint8)
		//WhateverId = BlackboardComp->GetKeyID("Whatever");
		
		BehaviorComp->StartTree(*opponent->Behavior);
	}

	//generate deltas
	ptr = 0;
	int32 x = 0, y = 0, size = 1;
	for (int32 asdf = 0; asdf<50; asdf++)
	{
		x--;
		y--;
		int32 dir = 0;
		for (int32 w = 0; w<4; w++)
		{
			#define next {if (dir==0) x++; else if (dir==1) y++; else if (dir==2) x--; else y--;}
			next
			int s = size;
			do
			{
				deltaX.Add(x);
				deltaY.Add(y);
				if (s>0)
				next
				s--;
			} while (s >= 0);
			dir++;
			#undef next
		}
		size += 2;
	}
}

void ANationAI::ThinkAboutBuying()
{
	if (nation->buildings.Num() >= 8)
		return;
	//in the current state of the game pretty difficult to implement - there's no real reason to buy anything, is there?
	//so just alternate for now
	if (nation->GetPopulation() + 2 >= nation->GetMaxPopulation())
	{
		nextBuilding = AHouse::StaticClass();
		return;
	}
	
	if (buildingState == 0)
		nextBuilding = AFarm::StaticClass();
	else if (buildingState == 1)
		nextBuilding = AGoldMine::StaticClass();
	else if (buildingState == 2)
		nextBuilding = AStoneQuarry::StaticClass();
	else if (buildingState==3)
		nextBuilding = AWoodCutter::StaticClass();
}

ABuilding* ANationAI::BuildBuilding()
{
	FString name = ABuilding::GetBuildingNameFromClass(nextBuilding);
	int32 gold = ABuilding::GetBuildingGoldCosts(name);
	int32 rocks = ABuilding::GetBuildingRockCosts(name);
	int32 wood = ABuilding::GetBuildingWoodCosts(name);

	if (nation->gold < gold || nation->rocks < rocks || nation->wood < wood)
		return nullptr;

	nation->gold -= gold;
	nation->rocks -= rocks;
	nation->wood -= wood;

	FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
	RV_TraceParams.bTraceComplex = true;
	RV_TraceParams.bTraceAsyncScene = true;
	RV_TraceParams.bReturnPhysicalMaterial = false;
	FHitResult RV_Hit, RV_Hit2;

	//the following assumes that nation->buildings[0] is not null!

	FVector origin, box;
	nation->buildings[0]->GetActorBounds(false, origin, box);
	int width = box.X;
	int depth = box.Y;

	FVector scale = nation->buildings[0]->GetActorScale3D(), location;
	do
	{
		ptr++;
		location = nation->buildings[0]->GetActorLocation();

		location.X = location.X + deltaX[ptr] * 3 * width*scale.X + FMath::RandRange(-width / 2, width / 2);
		location.Y = location.Y + deltaY[ptr] * 3 * depth*scale.Y + FMath::RandRange(-depth / 2, depth / 2);

		FHitResult RV_Hit(ForceInit), RV_Hit2(ForceInit);

		GetWorld()->LineTraceSingle(RV_Hit, location - box, location + box, ECC_Pawn, RV_TraceParams);
		GetWorld()->LineTraceSingle(RV_Hit2, location - FVector(-box.X, -box.Y, box.Z), location + FVector(-box.X, -box.Y, box.Z), ECC_Pawn, RV_TraceParams);

		//I have no idea what I'm doing...
		//it only work when writing it the following way. In case I put the if condition instead of the while (true), it stops working... dafuq

		if ((RV_Hit.bBlockingHit && (Cast<ABuilding>(RV_Hit.GetActor()) || Cast<APerson>(RV_Hit.GetActor()))) || (RV_Hit2.bBlockingHit && (Cast<ABuilding>(RV_Hit2.GetActor()) || Cast<APerson>(RV_Hit2.GetActor()))))
			;
		else
			break;
	} while (true);

	ABuilding *building = GetWorld()->SpawnActor<ABuilding>(nextBuilding);

	building->SetActorLocation(location);
	building->SetActorScale3D(scale);

	building->nation = nation;

	building->OnPlaced();

	nation->buildings.Add(building);

	return building;
}

void ANationAI::BuildVillagers()
{
	if (!buyVillager)
		return;
	buyVillager=false;
	buildingState = (buildingState+1)&3;
	
	if (!APerson::CanAfford(nation))
		return;
	
	APerson::Buy(nation);
	
	//again, assuming buildings[0] != null
	nation->buildings[0]->SpawnVillagerBeside();
}

void ANationAI::UpdateInfoLibrary()
{
	if (!populationMonitor)
	{
		populationMonitor = NewObject<UPopulationNeedsMonitor>();
		populationMonitor->Initialize(3, nation->persons.Num(), nation->maxpopulation);
	}
	else
	{
		populationMonitor->SetNewMax(nation->maxpopulation);
		populationMonitor->SetNewValue(nation->persons.Num());
	}


	if (!foodMonitor)
	{
		foodMonitor = NewObject<UResourceNeedsMonitor>();
		foodMonitor->Initialize(3, nation->food);
	}
	else
	{
		foodMonitor->SetNewValue(nation->food);
	}

	if (!goldMonitor)
	{
		goldMonitor = NewObject<UResourceNeedsMonitor>();
		goldMonitor->Initialize(3, nation->gold);
	}
	else
	{
		goldMonitor->SetNewValue(nation->gold);
	}

	if (!stoneMonitor)
	{
		stoneMonitor = NewObject<UResourceNeedsMonitor>();
		stoneMonitor->Initialize(3, nation->rocks);
	}
	else
	{
		stoneMonitor->SetNewValue(nation->rocks);
	}

	if (!woodMonitor)
	{
		woodMonitor = NewObject<UResourceNeedsMonitor>();
		woodMonitor->Initialize(3, nation->wood);
	}
	else
	{
		woodMonitor->SetNewValue(nation->wood);
	}
}

void ANationAI::ReconsiderObjectives()
{
	for (UObjective* objective : objectives)
	{
		UEconomicObjective* economicObjective = Cast<UEconomicObjective>(objective);
		if (economicObjective)
		{
			int32 priority = 0;
			if (economicObjective->Resource == EResourceEnum::RE_Food)
			{
				priority = foodMonitor->GetPriority();
			}
			else if (economicObjective->Resource == EResourceEnum::RE_Gold)
			{
				priority = goldMonitor->GetPriority();
			}
			else if (economicObjective->Resource == EResourceEnum::RE_Population)
			{
				priority = populationMonitor->GetPriority();
			}
			else if (economicObjective->Resource == EResourceEnum::RE_Stone)
			{
				priority = stoneMonitor->GetPriority();
			}
			else if (economicObjective->Resource == EResourceEnum::RE_Wood)
			{
				priority = woodMonitor->GetPriority();
			}

			if (priority <= 2){
				economicObjective->Importance = EImportanceEnum::IE_Low;
				economicObjective->Goal = EResourceObjectiveEnum::RE_Hold;
			}
			else if (priority <= 3){
				economicObjective->Importance = EImportanceEnum::IE_Moderate;
				economicObjective->Goal = EResourceObjectiveEnum::RE_Hold;
			}
			else if (priority <= 4){
				economicObjective->Importance = EImportanceEnum::IE_Moderate;
				economicObjective->Goal = EResourceObjectiveEnum::RE_Improve;
			}
			else if (priority <= 7){
				economicObjective->Importance = EImportanceEnum::IE_Important;
				economicObjective->Goal = EResourceObjectiveEnum::RE_Improve;
			}
			else{
				economicObjective->Importance = EImportanceEnum::IE_VeryImportant;
				economicObjective->Goal = EResourceObjectiveEnum::RE_ImproveDrastically;
			}
		}
		//other objectives here
	}

	SortObjectives();
}

void ANationAI::InitiateObjectives()
{
	objectives = TArray<UObjective*>();
	InitiateResourceObjectives();
}

void ANationAI::InitiateResourceObjectives()
{
	UEconomicObjective* tempGold = NewObject<UEconomicObjective>();
	tempGold->Resource = EResourceEnum::RE_Gold;
	tempGold->Goal = EResourceObjectiveEnum::RE_Hold;

	objectives.Add(tempGold);
	UEconomicObjective* tempWood = NewObject<UEconomicObjective>();
	tempWood->Resource = EResourceEnum::RE_Wood;
	tempWood->Goal = EResourceObjectiveEnum::RE_Hold;

	objectives.Add(tempWood);
	UEconomicObjective* tempFood = NewObject<UEconomicObjective>();
	tempFood->Resource = EResourceEnum::RE_Food;
	tempFood->Goal = EResourceObjectiveEnum::RE_Hold;

	objectives.Add(tempFood);
	UEconomicObjective* tempPopulation = NewObject<UEconomicObjective>();
	tempPopulation->Resource = EResourceEnum::RE_Population;
	tempPopulation->Goal = EResourceObjectiveEnum::RE_Hold;

	objectives.Add(tempPopulation);
	UEconomicObjective* tempStone = NewObject<UEconomicObjective>();
	tempStone->Resource = EResourceEnum::RE_Stone;
	tempStone->Goal = EResourceObjectiveEnum::RE_Hold;

	objectives.Add(tempStone);
	for (UObjective* objective : objectives)
	{
		objective->Importance = EImportanceEnum::IE_Moderate;
	}
}

void ANationAI::SortObjectives()
{
	objectives.Sort();
}

void ANationAI::ActUponObjectives()
{
	if (nation->persons.Num() <= 1)
	{
		int temp = 2 - nation->persons.Num();
		bool builtVillager = false;
		for (ABuilding* building : nation->buildings)
		{
			ATownCenter* townCenter = Cast<ATownCenter>(building);
			if (townCenter)
			{
				for (int32 i = 0; i < temp; i++)
				{
					if (nation->GetPopulation() + 1 < nation->maxpopulation)
					{
						if (APerson::CanAfford(nation))
						{
							APerson* person = townCenter->SpawnVillagerBeside();
							person->Buy(nation);
						}
					}
					else
					{
						populationMonitor->SetPriorityLevel(populationMonitor->GetPriorityLevel() + 1);
					}
				}
			}
		}
	}
	for (UObjective* objective : objectives)
	{
		UEconomicObjective* ecoObjective = Cast<UEconomicObjective>(objective);
		if (ecoObjective)
		{
			ActUponEconomicObjective(ecoObjective);
		}
		//TODO: Other Objectives here
	}
	
}

void ANationAI::AITick()
{
	if (GetWorld()->GetGameState<ALocothGameState>()->IsNewDay(myTime))
	{
		UpdateInfoLibrary();
		ReconsiderObjectives();
		ActUponTreatiesAndTrade();
		ActUponObjectives();
	}
}

void ANationAI::ReceiveTreatyRequest(UTreaty *treaty){
	//treaties.Add(treaty); 
	treaty->AcceptTreaty();
}

void ANationAI::ReceiveTradeRequest(UTradeRequest *trade)
{
	tradeRequests.Add(trade);
}

void ANationAI::ActUponTreatiesAndTrade()
{
	//TODO: Advanced AI
	for (UTreaty* treaty : treaties)
	{
		int32 tmp = FMath::FRandRange(0, 1);
		if (tmp == 1)
		{
			treaty->AcceptTreaty();
		}
		else
		{
			treaty->DenyTreaty();
		}
	}


	for (UTradeRequest* tradeRequest : tradeRequests)
	{
		if (CanAffordTrade(tradeRequest))
		{
			tradeRequest->Accept();
			tradeRequests.Remove(tradeRequest);
		}
		else
		{
			tradeRequest->Deny();
			tradeRequests.Remove(tradeRequest);
		}
		delete tradeRequest;
	}
}

bool ANationAI::CanAffordTrade(UTradeRequest* tradeReq)
{
	if (tradeReq->goldTake < nation->gold / 3 && tradeReq->woodTake < nation->wood / 3 && tradeReq->rocksTake < nation->rocks / 3 && tradeReq->foodTake < nation->food / 3)
	{
		for (UObjective* objective : objectives)
		{
			UEconomicObjective* ecoObjective = Cast<UEconomicObjective>(objective);
			if (ecoObjective)
			{
				if (ecoObjective->Resource == EResourceEnum::RE_Food && tradeReq->foodTake > 0)
				{
					if (ecoObjective->Goal == EResourceObjectiveEnum::RE_Improve || ecoObjective->Goal == EResourceObjectiveEnum::RE_ImproveDrastically)
						return false;
				}
				else if (ecoObjective->Resource == EResourceEnum::RE_Wood && tradeReq->woodTake > 0)
				{
					if (ecoObjective->Goal == EResourceObjectiveEnum::RE_Improve || ecoObjective->Goal == EResourceObjectiveEnum::RE_ImproveDrastically)
						return false;
				}
				else if (ecoObjective->Resource == EResourceEnum::RE_Gold && tradeReq->goldTake > 0)
				{
					if (ecoObjective->Goal == EResourceObjectiveEnum::RE_Improve || ecoObjective->Goal == EResourceObjectiveEnum::RE_ImproveDrastically)
						return false;
				}
				else if (ecoObjective->Resource == EResourceEnum::RE_Stone && tradeReq->rocksTake > 0)
				{
					if (ecoObjective->Goal == EResourceObjectiveEnum::RE_Improve || ecoObjective->Goal == EResourceObjectiveEnum::RE_ImproveDrastically)
						return false;
				}
			}
		}
		return true;
	} 
	return false;
}

void ANationAI::ActUponEconomicObjective(UEconomicObjective* ecoObjective)
{
	//Build Building
	//Send villager to work
	if (ecoObjective->Goal == EResourceObjectiveEnum::RE_Improve || ecoObjective->Goal == EResourceObjectiveEnum::RE_ImproveDrastically)
	{
		bool doneSomething = false;
		if (ecoObjective->Resource == EResourceEnum::RE_Food)
		{
			for (int i = 0; !doneSomething && i < nation->buildings.Num(); i++)
			{
				AFarm* farm = Cast<AFarm>(nation->buildings[i]);
				if (farm && !farm->IsLastLevel() && farm->CanAfford(nation->rocks, nation->wood, nation->gold))
				{
					farm->Upgrade();
					nation->persons[0]->GetToWork(farm);
					doneSomething = true;
				}
			}
			if (!doneSomething)
			{
				nextBuilding = AFarm::StaticClass();
				ABuilding* building = BuildBuilding();
				if (building)
					nation->persons[0]->GetToWork(building);

			}
		}
		else if (ecoObjective->Resource == EResourceEnum::RE_Wood)
		{
			for (int i = 0; !doneSomething && i < nation->buildings.Num(); i++)
			{
				AWoodCutter* woodcutter = Cast<AWoodCutter>(nation->buildings[i]);
				if (woodcutter && !woodcutter->IsLastLevel() && woodcutter->CanAfford(nation->rocks, nation->wood, nation->gold))
				{
					woodcutter->Upgrade();
					nation->persons[0]->GetToWork(woodcutter);
					doneSomething = true;
				}
			}
			if (!doneSomething)
			{
				nextBuilding = AWoodCutter::StaticClass();
				ABuilding* building = BuildBuilding();
				if (building)
					nation->persons[0]->GetToWork(building);
			}
		}
		else if (ecoObjective->Resource == EResourceEnum::RE_Gold)
		{
			for (int i = 0; !doneSomething && i < nation->buildings.Num(); i++)
			{
				AGoldMine* goldmine = Cast<AGoldMine>(nation->buildings[i]);
				if (goldmine && !goldmine->IsLastLevel() && goldmine->CanAfford(nation->rocks, nation->wood, nation->gold))
				{
					goldmine->Upgrade();
					nation->persons[0]->GetToWork(goldmine);
					doneSomething = true;
				}
			}
			if (!doneSomething)
			{
				nextBuilding = AGoldMine::StaticClass();
				ABuilding* building = BuildBuilding();
				if (building)
					nation->persons[0]->GetToWork(building);
			}
		}
		else if (ecoObjective->Resource == EResourceEnum::RE_Population)
		{
			nextBuilding = AHouse::StaticClass();
			ABuilding* building = BuildBuilding();
		}
		else if (ecoObjective->Resource == EResourceEnum::RE_Stone)
		{
			for (int i = 0; !doneSomething && i < nation->buildings.Num(); i++)
			{
				AStoneQuarry* stoneQuarry = Cast<AStoneQuarry>(nation->buildings[i]);
				if (stoneQuarry && !stoneQuarry->IsLastLevel() && stoneQuarry->CanAfford(nation->rocks, nation->wood, nation->gold))
				{
					stoneQuarry->Upgrade();
					nation->persons[0]->GetToWork(stoneQuarry);
					doneSomething = true;
				}
			}
			if (!doneSomething)
			{
				nextBuilding = AStoneQuarry::StaticClass();
				ABuilding* building = BuildBuilding();
				if (building)
					nation->persons[0]->GetToWork(building);
			}
		}
	}
}