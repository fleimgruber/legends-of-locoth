#include "LegendsOfLocoth.h"
#include "../Controller/LocothPlayerController.h"
#include "../Notifications/NotificationData.h"
#include "University.h"


void AUniversity::NewDay()
{
	Super::NewDay();
	if (toResearchList.Num() && GetWorkerCount())
		if (toResearchList[0]->GetPropability()*sqrt(GetWorkerCount()) >= FMath::FRandRange(0, 10000))
		{
			USkill* tmp = toResearchList.Pop();
			if (tmp->UpdateSettings(nation->nationId))
			{
				ALocothPlayerController *playerController = Cast<ALocothPlayerController>(GetWorld()->GetFirstPlayerController());
				if (nation == playerController->nation) //don't display a notification when the opponent discovers something
				{
					auto n=NewObject<UNotificationData>();
					n->SetTitle("New skill discovered!");
					n->SetSubtitle(tmp->GetName());
					playerController->ShowNotification(n);
				}
			}
		}
}

void AUniversity::LoadToResearchList(TArray<USkill*> &alreadyDiscovered)
{
	AddSkill(alreadyDiscovered, NewObject<USteelTools>());
	AddSkill(alreadyDiscovered, NewObject<UBetterScythe>());
	AddSkill(alreadyDiscovered, NewObject<UCrystals>());
	AddSkill(alreadyDiscovered, NewObject<UGoDeeper>());
	AddSkill(alreadyDiscovered, NewObject<USteelWeapons>());
	AddSkill(alreadyDiscovered, NewObject<UThreeFieldCropRotation>());
}

AUniversity::AUniversity(const FObjectInitializer& ObjectInitialize)
	: ABuilding(ObjectInitialize, "generalUniversityModel")
{
	name = GetNameStatic();	
}

void AUniversity::ShuffleSkills()
{
	for (int32 i=toResearchList.Num()-1; i>0; i--)
		toResearchList.Swap(i,FMath::RandRange(0,i-1));	
}

void AUniversity::AddSkill(TArray<USkill*> &alreadyDiscovered, USkill *skill)
{
	bool in=false;
	for (auto x : alreadyDiscovered)
		in|= x->GetName() == skill->GetName();
	if (!in)
		toResearchList.Add(skill);
}