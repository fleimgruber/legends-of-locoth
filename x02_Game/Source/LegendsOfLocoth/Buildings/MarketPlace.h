// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Building.h"
#include "MarketPlace.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API AMarketPlace : public ABuilding
{
	GENERATED_BODY()
	
public:
	AMarketPlace(const FObjectInitializer& ObjectInitializer);
	
	/**
	* called every day
	*/
	virtual void NewDay() override;

	/**
	* returns the building name
	*\return building name as FString
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	static FString GetNameStatic() { return "Market Place"; };
	
};
