// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Building.h"
#include "../Controller/LocothPlayerController.h"
#include "../Buildings/Barracks.h"
#include "../Buildings/Farm.h"
#include "../Buildings/University.h"
#include "../Buildings/GoldMine.h"
#include "../Buildings/Harbour.h"
#include "../Buildings/House.h"
#include "../Buildings/MarketPlace.h"
#include "../Buildings/StoneQuarry.h"
#include "../Buildings/WoodCutter.h"
#include "../Buildings/TownCenter.h"



ABuilding::ABuilding(const FObjectInitializer& ObjectInitialize, FString meshName) : AStaticMeshActor(ObjectInitialize)
{
	name = "untitled";
	IsPlaced = false;
	level = 1;
	baseEfficientcy = 2;
	
	health = 100;
	
	FString meshPath = FString(TEXT("/Game/levels/game/Buildings/Meshes/"))+meshName;
	
	ConstructorHelpers::FObjectFinder<UStaticMesh> mesh(*meshPath); //there's an overloaded * operator for FString -> TCHAR*....
	UStaticMeshComponent *staticMesh = GetStaticMeshComponent();
	staticMesh->SetStaticMesh(mesh.Object);
	staticMesh->SetMobility(EComponentMobility::Movable);
	staticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	
	staticMesh->OnClicked.AddDynamic(this, &ABuilding::OnClicked);
}

void ABuilding::OnClicked(UPrimitiveComponent* pComponent)
{
	ALocothPlayerController *playerController = Cast<ALocothPlayerController>(GetWorld()->GetFirstPlayerController());
	if (nation == playerController->nation) //it's the players building
		playerController->ShowControllerHUD(this);
}

void ABuilding::OnPlaced()
{
	IsPlaced = true;
	GetStaticMeshComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	
	Cast<ALocothPlayerController>(GetWorld()->GetFirstPlayerController())->AddHealthBar(this);
}

ABuilding::ABuilding(const FObjectInitializer& ObjectInitialize) : ABuilding(ObjectInitialize, "DefaultBuilding") { }

FString ABuilding::GetName()
{
	return name;
}

uint8 ABuilding::GetLevel()
{
	return level;
}

void ABuilding::SetLevel(uint8 lvl)
{
	this->baseEfficientcy += (lvl-level)*3;
	this->level = lvl;
}

int32 ABuilding::GetBaseEfficientcy()
{
	return baseEfficientcy;
}

void ABuilding::SetBaseEfficientcy(int32 efficientcy)
{
	this->baseEfficientcy = efficientcy;
}

void ABuilding::NewDay()
{
	//feed workers
	for (int32 i=0; i<GetWorkerCount(); i++)
		if (APerson::ConsumeFood(nation))
			workerCount--;
}

void ABuilding::AddPerson(APerson *person)
{
	nation->persons.Remove(person);
	person->Destroy();
	workerCount++;
}

void ABuilding::RemovePerson()
{
	workerCount--;
	SpawnVillagerBeside();
}

void ABuilding::DeleteBuilding(){
	while (GetWorkerCount())
		RemovePerson();
	nation->buildings.Remove(this);
}

int32 ABuilding::GetWorkerCount()
{
	return workerCount;
}

bool ABuilding::CanAfford(int32 rocks, int32 wood, int32 gold){
	int32 urocks = 0;
	int32 uwood = 0;
	int32 ugold = 0;

	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, this->level + 1, GetName());
	if (urocks == 0 && uwood == 0 && ugold == 0)
		return false;
	return rocks >= urocks && wood >= uwood && gold >= ugold;
}

bool ABuilding::IsLastLevel()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, this->level + 1, GetName());
	return urocks == 0 && uwood == 0 && ugold == 0;
}

void ABuilding::Upgrade()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, this->level + 1, GetName());
	SetLevel(level+1);
	nation->rocks -= urocks;
	nation->wood -= uwood;
	nation->gold -= ugold;
}

int32 ABuilding::GetUpgradeGold()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, this->level + 1, GetName());
	return ugold;
}

int32 ABuilding::GetUpgradeWood()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, this->level + 1, GetName());
	return uwood;
}

int32 ABuilding::GetUpgradeRocks()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, this->level + 1, GetName());
	return urocks;
}

int32 ABuilding::GetBuildingGoldCosts(FString fName)
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, fName);
	return ugold;
}

int32 ABuilding::GetBuildingRockCosts(FString fName)
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, fName);
	return urocks;
}

int32 ABuilding::GetBuildingWoodCosts(FString fName)
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, fName);
	return uwood;
}

FString ABuilding::GetBuildingNameFromClass(TSubclassOf<ABuilding> building)
{
	//I do not like it either
	
	if (building==ABarracks::StaticClass())
		return ABarracks::GetNameStatic();
	if (building==AFarm::StaticClass())
		return AFarm::GetNameStatic();
	if (building==AUniversity::StaticClass())
		return AUniversity::GetNameStatic();
	if (building==AGoldMine::StaticClass())
		return AGoldMine::GetNameStatic();
	if (building==AHarbour::StaticClass())
		return AHarbour::GetNameStatic();
	if (building==AHouse::StaticClass())
		return AHouse::GetNameStatic();
	if (building==AMarketPlace::StaticClass())
		return AMarketPlace::GetNameStatic();
	if (building==AStoneQuarry::StaticClass())
		return AStoneQuarry::GetNameStatic();
	if (building==ATownCenter::StaticClass())
		return ATownCenter::GetNameStatic();
	if (building==AWoodCutter::StaticClass())
		return AWoodCutter::GetNameStatic();
	return "";
}

APerson *ABuilding::SpawnVillagerBeside()
{
	FVector location = GetActorLocation();
	FVector origin,box;
	GetActorBounds(false, origin, box);
	location.Y += box.Y*2;
	return nation->SpawnVillager(location, FRotator(0,0,0), FVector(3,3,3));
}

ASoldier *ABuilding::SpawnSoldierBeside()
{
	FVector location = GetActorLocation();
	FVector origin,box;
	GetActorBounds(false, origin, box);
	location.Y += box.Y*2;
	return nation->SpawnSoldier(location, FRotator(0,0,0), FVector(3,3,3));
}

void ABuilding::SetWorkerCount(int32 value)
{
	workerCount = value;
}


int32 ABuilding::GetHealth()
{
	return this->health;
}

void ABuilding::SetHealth(int32 iHealth)
{
	this->health = iHealth;
}

void ABuilding::Attack(int32 power)
{
	this->health = FMath::Max(0, this->health - power);
	if (health == 0)
	{
		DeleteBuilding();
		Destroy();
	}
}

void ABuilding::Repair()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, GetName());

	int32 repairWood = static_cast<int32>(static_cast<double>(uwood)* static_cast<double>(100 - this->health) / 100);
	int32 repairRocks = static_cast<int32>(static_cast<double>(urocks)* static_cast<double>(100 - this->health) / 100);
	int32 repairGold = static_cast<int32>(static_cast<double>(ugold)* static_cast<double>(100 - this->health) / 100);

	this->nation->wood -= repairWood;
	this->nation->rocks -= repairRocks;
	this->nation->gold -= repairGold;

	this->health = 100;
}

int32 ABuilding::GetRepairGold()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, GetName());
	return static_cast<int32>(static_cast<double>(ugold) * static_cast<double>(100 - this->health) / 100);
}

int32 ABuilding::GetRepairWood()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, GetName());
	return static_cast<int32>(static_cast<double>(uwood)* static_cast<double>(100 - this->health) / 100);
}

int32 ABuilding::GetRepairRocks()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, GetName());
	return static_cast<int32>(static_cast<double>(urocks)* static_cast<double>(100 - this->health) / 100);
}

bool ABuilding::CanAffordRepair()
{
	int32 urocks, uwood, ugold;
	ULocothXmlLoadingController::GetUpgradeInfo(urocks, uwood, ugold, 1, GetName());

	int32 repairWood = static_cast<int32>(static_cast<double>(uwood)* static_cast<double>(100 - this->health) / 100);
	int32 repairRocks = static_cast<int32>(static_cast<double>(urocks)* static_cast<double>(100 - this->health) / 100);
	int32 repairGold = static_cast<int32>(static_cast<double>(ugold)* static_cast<double>(100 - this->health) / 100);

	return repairWood <= nation->wood && repairRocks <= nation->rocks
		&& repairGold <= nation->gold && this->health < 100;
}

FString ABuilding::GetRepairCostsAsString()
{
	FString ret = TEXT("Repair costs: ");
	ret.AppendInt(GetRepairGold());
	ret.Append(TEXT("G, "));
	ret.AppendInt(GetRepairWood());
	ret.Append(TEXT("W, "));
	ret.AppendInt(GetRepairRocks());
	ret.Append(TEXT("R"));

	return ret;
}