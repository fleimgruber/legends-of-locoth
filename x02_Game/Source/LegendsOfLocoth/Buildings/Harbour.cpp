// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Harbour.h"

AHarbour::AHarbour(const FObjectInitializer& ObjectInitialize)
	: ABuilding(ObjectInitialize, "harbourModel")
{
	name = GetNameStatic();
}

void AHarbour::NewDay()
{
	Super::NewDay();
    //you can buy ships here, so nothing to do
}

