// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "TownCenter.h"

ATownCenter::ATownCenter(const FObjectInitializer& ObjectInitialize) 
	: ABuilding(ObjectInitialize, "townHallModel")
{
	name = GetNameStatic();
}
