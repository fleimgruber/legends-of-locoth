// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Buildings/Building.h"
#include "TownCenter.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API ATownCenter : public ABuilding
{
	GENERATED_BODY()
	
public:
	ATownCenter(const FObjectInitializer& ObjectInitialize);
	
	/**
	* returns the building name
	*\return building name as FString
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	static FString GetNameStatic() { return "Town Center"; };
	
};
