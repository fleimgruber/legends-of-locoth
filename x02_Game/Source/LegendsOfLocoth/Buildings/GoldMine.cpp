// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "GoldMine.h"
#include <cmath>
#include <UnrealMathUtility.h>


AGoldMine::AGoldMine(const FObjectInitializer& ObjectInitialize)
	: ABuilding(ObjectInitialize, "goldMineModel")
{
    name = GetNameStatic();
}

void AGoldMine::NewDay()
{
    if (!IsPlaced)
        return;
	Super::NewDay();
	
	float root=sqrt(GetWorkerCount());
	
	int32 a=(int32)root;
	/*
	nation->gold += baseEfficientcy*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->gold += baseEfficientcy;*/

	UBuildingResearch* res = UBuildingResearch::GetInstance(name, nation->nationId);
	int32 eff = baseEfficientcy + res->GetEfficiency();
	nation->gold += eff*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->gold += eff;
}
