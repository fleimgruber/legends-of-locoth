// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Building.h"
#include "Barracks.generated.h"

/**
 *
 */
UCLASS()
class LEGENDSOFLOCOTH_API ABarracks : public ABuilding
{
	GENERATED_BODY()

public:
	ABarracks(const FObjectInitializer& ObjectInitializer);

	static const int32 SOLDIER_PRICE;

	/**
	* Gets the price for buying one soldier.
	*
	* \return soldier price
	*/
	UFUNCTION(BlueprintCallable, Category = Barracks)
	int32 GetSoldierPrice();

	/**
	* buys a soldier, the price depends on the building level
	*/
	UFUNCTION(BlueprintCallable, Category = Barracks)
	void BuySoldier();

	/**
	* checks if buying a soldier can be afforded
	*/
	UFUNCTION(BlueprintCallable, Category = Barracks)
	bool CanAffordSoldier();

	/**
	* returns the building name
	*\return building name as FString
	*/
	UFUNCTION(BlueprintCallable, Category = Building)
		static FString GetNameStatic() { return "Barracks"; };
};
