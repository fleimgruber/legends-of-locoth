#pragma once

#include "Building.h"
#include "../Skills/Skill.h"
#include <cmath>
#include "../Notifications/NotificationData.h"
#include "../Skills/BetterScythe.h"
#include "../Skills/ThreeFieldCropRotation.h"
#include "../Skills/Crystals.h"
#include "../Skills/GoDeeper.h"
#include "../Skills/SteelTools.h"
#include "../Skills/SteelWeapons.h"
#include "University.generated.h"


UCLASS()
class LEGENDSOFLOCOTH_API AUniversity : public ABuilding
{
	GENERATED_BODY()


	
public:

	AUniversity(const FObjectInitializer& ObjectInitialize);
	
	
	/**
	 * returns the building name
	 *\return building name as FString
	 */
	UFUNCTION(BlueprintCallable, Category=Building)
	static FString GetNameStatic() { return "University"; };

	/**
	* Checks whether this day some new skill is developed in this country
	*/
	virtual void NewDay() override;


	UPROPERTY()
	TArray<USkill*> toResearchList;

	/**
	* Loads the skills the according university can research.
	*/
	void LoadToResearchList(TArray<USkill*> &alreadyDiscovered);
	
	/**
	* Adds a skill, in case it in not already discovered, to the research list
	*/
	void AddSkill(TArray<USkill*> &alreadyDiscovered, USkill *skill);
	
	/**
	* Random shuffle skills
	*/
	void ShuffleSkills();
};
