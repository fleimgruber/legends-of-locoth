// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "StoneQuarry.h"
#include <UnrealMathUtility.h>
#include <cmath>


AStoneQuarry::AStoneQuarry(const FObjectInitializer& ObjectInitialize)
	: ABuilding(ObjectInitialize, "stoneQuarryModel")
{
    name = GetNameStatic();
}


void AStoneQuarry::NewDay()
{
    if (!IsPlaced)
        return;

	Super::NewDay();
	
	float root=sqrt(GetWorkerCount());
	int32 a=(int32)root;
	/*
	nation->rocks += baseEfficientcy*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->rocks += baseEfficientcy;*/

	UBuildingResearch* res = UBuildingResearch::GetInstance(name, nation->nationId);
	int32 eff = baseEfficientcy + res->GetEfficiency();
	nation->rocks += eff*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->rocks += eff;
}
