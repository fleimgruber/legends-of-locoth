// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Farm.h"
#include <cmath>
#include <UnrealMathUtility.h>


AFarm::AFarm(const FObjectInitializer& ObjectInitialize) 
	: ABuilding(ObjectInitialize, "farmModel")
{
    name = GetNameStatic();
}

void AFarm::NewDay()
{
    if (!IsPlaced)
        return;
	Super::NewDay();
	
	float root=sqrt(GetWorkerCount());
	int32 a=(int32)root;
	/*nation->food += baseEfficientcy*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->food += baseEfficientcy;*/
	UBuildingResearch* res = UBuildingResearch::GetInstance(name, nation->nationId);
	int32 eff = baseEfficientcy + res->GetEfficiency();
	nation->food += eff*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->food += eff;
}
