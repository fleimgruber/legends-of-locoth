// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Building.h"
#include "Farm.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API AFarm : public ABuilding
{
	GENERATED_BODY()
	
public:
	AFarm(const FObjectInitializer& ObjectInitialize);
    
	/**
	* called every day
	*/
	virtual void NewDay() override;

	/**
	* returns the building name
	*\return building name as FString
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	static FString GetNameStatic() { return "Farm"; };
	
};
