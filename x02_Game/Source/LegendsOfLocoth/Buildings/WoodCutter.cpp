// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "WoodCutter.h"
#include <cmath>
#include <UnrealMathUtility.h>

AWoodCutter::AWoodCutter(const FObjectInitializer& ObjectInitialize)
	: ABuilding(ObjectInitialize, "woodCutterModel")
{
    name = GetNameStatic();
}

void AWoodCutter::NewDay()
{
    if (!IsPlaced)
        return;
	Super::NewDay();
	
	float root=sqrt(GetWorkerCount());
	int32 a=(int32)root;
	/*
	nation->wood += baseEfficientcy*a;
	if (FMath::FRandRange(0,1)<root - a)
		nation->wood += baseEfficientcy;*/

	UBuildingResearch* res = UBuildingResearch::GetInstance(name, nation->nationId);
	int32 eff = baseEfficientcy + res->GetEfficiency();
	nation->wood += eff*a;
	if (FMath::FRandRange(0, 1)<root - a)
		nation->wood += eff;
}