// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/StaticMeshActor.h"
#include "../Controller/Nation.h"
#include "../Pawns/Soldier.h"
#include "../Pawns/Person.h"
#include "../XmlController/LocothXmlLoadingController.h"
#include "../Skills/Skill.h"
#include "Building.generated.h"


UCLASS()
class LEGENDSOFLOCOTH_API ABuilding : public AStaticMeshActor
{
	GENERATED_BODY()
	
protected:
	UPROPERTY()
	FString name;
	
	uint8 level;
	int32 baseEfficientcy;
	int workerCount;

	UPROPERTY()
	int32 health;

public:

	UPROPERTY(BlueprintReadWrite)
	UNation *nation;
	
	UPROPERTY(BlueprintReadWrite)
	bool IsPlaced;
	
	ABuilding(const FObjectInitializer& ObjectInitialize); //unreal engine needs this constructor
	
	/**
	* Constructor loading the given mesh as StaticMesh
	*
	* \param ObjectInitialize
	* \param mesh is the buildingMesh
	*/
	ABuilding(const FObjectInitializer& ObjectInitialize, FString mesh);
	
	/**
	* checks if the next update can be bought using the given amount of resources
	*
	* \param rocks are the amount of rocks available
	* \param wood is the amount of wood available
	* \param gold is the amount of gold available
	* \return true if next update can be bought
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	bool CanAfford(int32 rocks, int32 wood, int32 gold);
	
	/**
	* checks if the building is on last level
	* \return true if building is on last level
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	bool IsLastLevel();
	
	/**
	* upgrade this building to the next level
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	void Upgrade();

	/**
	* returns the corresponding building costs for gold
	* \return building costs for gold
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	static int32 GetBuildingGoldCosts(FString fName);

	/**
	* returns the corresponding building costs for rocks
	* \return building costs for rocks
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	static int32 GetBuildingRockCosts(FString fName);

	/**
	* returns the corresponding building costs for wood
	* \return building costs for wood
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	static int32 GetBuildingWoodCosts(FString fName);
	
	/**
	* returns the corresponding update costs for gold
	* \return update costs for gold
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	int32 GetUpgradeGold();

	/**
	* returns the corresponding update costs for wood
	* \return update costs for wood
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	int32 GetUpgradeWood();

	/**
	* returns the corresponding update costs for rocks
	* \return update costs for rocks
	*/
	UFUNCTION(BlueprintCallable, Category = Upgrades)
	int32 GetUpgradeRocks();
	
	/**
	* returns the building name
	* \return building name
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	FString GetName();
	
	/**
	* returns the building level
	* \return building level
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	uint8 GetLevel();
	
	/**
	* sets the building level
	* \param level that building should be set to
	*/
	void SetLevel(uint8 lvl);
	
	/**
	* returns the buildings base efficiency
	* \return base efficiency
	*/
	int32 GetBaseEfficientcy();
	
	/**
	* sets the buildings base efficiency
	* \param efficiency that should be set
	*/
	void SetBaseEfficientcy(int32 efficientcy);
	
	/**
	* called every day
	*/
	virtual void NewDay();

	/**
	* called when a new buildling is bought and placed on the map
	*/
	UFUNCTION(BlueprintCallable, Category = Building)
	virtual void OnPlaced();
	
	/**
	* called when the building is clicked
	* \param UPrimitiveComponent
	*/
	UFUNCTION()
	void OnClicked(UPrimitiveComponent* pComponent);

	/**
	* adds a person to work in the building
	* \param Person to be added
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	void AddPerson(APerson *person);
	
	/**
	* removes a person to work in the building
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	void RemovePerson();

	/**
	* removes a building and sets its workers free
	*/
	UFUNCTION(BlueprintCallable, Category = Building)
	virtual void DeleteBuilding();
	
	/**
	* get number of people working in the building
	* \return people count
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	int32 GetWorkerCount();
	
	/**
	* get the building name from class as FString
	* \param building
	* \return building name from class as FString
	*/
	UFUNCTION(BlueprintCallable, Category = Building)
	static FString GetBuildingNameFromClass(TSubclassOf<ABuilding> building);
	
	/**
	* spaws a villager next to this building
	*
	* \return the villager created
	*/
	UFUNCTION(BlueprintCallable, Category = Villager)
	APerson *SpawnVillagerBeside();
	
	/**
	 * spaws a soldier next to this building
	 *
	 * \return the soldier created
	 */
	UFUNCTION(BlueprintCallable, Category = Soldier)
	ASoldier *SpawnSoldierBeside();
	
	/**
	* sets the number of people working here, should only be used when loading a game
	*/
	void SetWorkerCount(int32 value);

	/**
	* Returns the health of the building
	* \return the health of the building
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	int32 GetHealth();

	/**
	* Sets the health of the building
	* \param health of the building that should be set
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	void SetHealth(int32 iHealth);

	/**
	* Attacks the building, decreases the health of the building.
	* \param power the power of the attacker
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	void Attack(int32 power);

	/**
	* Repairs the actual building.
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	void Repair();

	/**
	* returns the corresponding repair costs for rocks
	* \return the amount of gold needed for repair
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	int32 GetRepairGold();

	/**
	* returns the corresponding repair costs for wood
	* \return the amount of wood needed for repair
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	int32 GetRepairWood();

	/**
	* returns the corresponding repair costs for rocks
	* \return the amount of rocks needed for repair
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	int32 GetRepairRocks();

	/**
	* Checks if the repair can be afforded.
	* \return if the afford can be afforded
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	bool CanAffordRepair();
	
	/**
	* Returns the repair costs as string.
	* \return the repair costs as string
	*/
	UFUNCTION(BlueprintCallable, Category = BuildingHealth)
	FString GetRepairCostsAsString();
};
