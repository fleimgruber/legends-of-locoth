// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Building.h"
#include "House.h"


const int32 AHouse::HOUSE_GAIN_SPACE = 5;

AHouse::AHouse(const FObjectInitializer& ObjectInitialize)
	:ABuilding(ObjectInitialize, "houseModel")
{
	name = GetNameStatic();
}

void AHouse::OnPlaced()
{
	Super::OnPlaced();
	nation->maxpopulation += AHouse::HOUSE_GAIN_SPACE;
}

void AHouse::DeleteBuilding()
{
	Super::DeleteBuilding();
	nation->maxpopulation -= AHouse::HOUSE_GAIN_SPACE;
}

int32 AHouse::GetGainSpaceConstant()
{
	return HOUSE_GAIN_SPACE;
}