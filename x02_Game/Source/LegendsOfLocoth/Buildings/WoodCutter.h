// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Building.h"
#include "WoodCutter.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API AWoodCutter : public ABuilding
{
	GENERATED_BODY()
	
public:
	AWoodCutter(const FObjectInitializer& ObjectInitialize);
    
	/**
	* called every day
	*/
	virtual void NewDay() override;

	/**
	* returns the building name
	*\return building name as FString
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	static FString GetNameStatic() { return "Wood Cutter"; };

};
