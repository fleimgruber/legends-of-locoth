// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Building.h"
#include "House.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API AHouse : public ABuilding
{
	GENERATED_BODY()
	
public:
	
	static const int32 HOUSE_GAIN_SPACE;
	
	AHouse(const FObjectInitializer& ObjectInitialize);

	/**
	* called when a new house is bought and placed on the map
	*/
	virtual void OnPlaced() override;
	
	/**
	* called when the building is destroyed
	*/
	virtual void DeleteBuilding() override;
	
	/**
	 * \return returns how much spaces buying a house gives you
	 */
	UFUNCTION(BlueprintCallable, Category=House)
	int32 GetGainSpaceConstant();
	
	/**
	* returns the building name
	*\return building name as FString
	*/
	UFUNCTION(BlueprintCallable, Category=Building)
	static FString GetNameStatic() { return "House"; };

};
