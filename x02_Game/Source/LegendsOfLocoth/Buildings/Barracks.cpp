// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Barracks.h"

const int32 ABarracks::SOLDIER_PRICE = 70;

ABarracks::ABarracks(const FObjectInitializer& ObjectInitializer) 
	: ABuilding(ObjectInitializer, "barracksModel")
{
	name = GetNameStatic();
}

void ABarracks::BuySoldier() {
	if (CanAffordSoldier()){
		if (level != 1) nation->gold -= (SOLDIER_PRICE - level * 10);
		else nation->gold -= SOLDIER_PRICE;
	}
}

int32 ABarracks::GetSoldierPrice(){
	if (level == 1) return SOLDIER_PRICE;
	else return SOLDIER_PRICE - level * 10;
}

bool ABarracks::CanAffordSoldier(){
	if (GetSoldierPrice() > nation->gold) return false;
	return true;
}