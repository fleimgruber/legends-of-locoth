#include "LegendsOfLocoth.h"
#include "Person.h"
#include "../Buildings/Building.h"
#include "../Controller/Nation.h"
#include "../LocothGameState.h"
#include "../Controller/LocothPlayerController.h"
#include "../Notifications/NotificationData.h"

const int32 APerson::GOLD_COSTS = 100;
const int32 APerson::FOOD_COSTS = 100;

APerson::APerson(const FObjectInitializer& ObjectInitialize) : ACharacter(ObjectInitialize)
{
	PrimaryActorTick.bCanEverTick = true;
	health = 100;
}

void APerson::MoveToLocation(FVector destination)
{
	onWayToWork = nullptr;
	UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
	NavSys->SimpleMoveToLocation(GetController(), destination);
}

void APerson::GetToWork(ABuilding *building)
{
	MoveToLocation(building->GetActorLocation());
	onWayToWork = building;
}

void APerson::Tick(float deltaT)
{
	Super::Tick(deltaT);
	if (IsPendingKill())
		return;
	
	if (onWayToWork && (GetActorLocation()-onWayToWork->GetActorLocation()).Size() < 500) //in case we're almost there
	{
		onWayToWork->AddPerson(this);
		onWayToWork = nullptr;
	}
	if (GetWorld()->GetGameState<ALocothGameState>()->IsNewDay(myTime))
	{
		if (ConsumeFood(nation))
		{
			nation->persons.Remove(this);
			Destroy();
		}
	}
}

bool APerson::ConsumeFood(UNation *nationX)
{
	if (nationX->food > 0)
	{
		nationX->food--;
		return false;
	}
	nationX->food += 100; //lisa likes eating humans xD
	ALocothPlayerController *playerController = Cast<ALocothPlayerController>(nationX->world->GetFirstPlayerController());
	if (playerController->nation == nationX)
	{
		auto n=NewObject<UNotificationData>();
		n->SetTitle("Starvin Marvin!");
		n->SetSubtitle("One of your people starved to death.");
		playerController->ShowNotification(n);
	}
	return true;
}

int32 APerson::GetGoldCosts() { return GOLD_COSTS; }

int32 APerson::GetFoodCosts() { return FOOD_COSTS; }

bool APerson::CanAfford(UNation *nationX)
{
	return nationX->gold >= GOLD_COSTS && nationX->food >= FOOD_COSTS;
}

void APerson::Buy(UNation *nationX)
{
	nationX->gold -= GOLD_COSTS;
	nationX->food -= FOOD_COSTS;
}

void APerson::Attack(int32 power)
{
	health = FMath::Max(0, health-power);
	if (health == 0)
	{
		nation->persons.Remove(this);
		Destroy();
	}
}
