#pragma once

#include "GameFramework/Character.h"
#include "Person.generated.h"

class ABuilding;
class UNation;

/**
 * This class is the basic-implementation of a visible person.
 * 
 * \class APerson
 */
UCLASS()
class LEGENDSOFLOCOTH_API APerson: public ACharacter
{
	GENERATED_BODY()
private:
	uint32 myTime;
	
protected:
	ABuilding *onWayToWork;

public:
	static const int32 GOLD_COSTS;
	static const int32 FOOD_COSTS;
	
	UPROPERTY(BlueprintReadOnly)
	int32 health;
	
	APerson(const FObjectInitializer& ObjectInitialize);

	UPROPERTY(BlueprintReadOnly)
	UNation *nation;

	/**
	 * This method moves the person to the given location.
	 * 
	 * \param destination is the destination where the person should move.
	 */
	UFUNCTION(BlueprintCallable, Category = Person)
	void MoveToLocation(FVector destination);
	
	/**
	 * This method sets the building in which the person should work.
	 *
	 * \param building is the building where the person should work.
	 */
	UFUNCTION(BlueprintCallable, Category = Person)
	void GetToWork(ABuilding *building);
	
	virtual void Tick(float delatT) override;
	
	UFUNCTION(BlueprintCallable, Category = Person)
	static int32 GetGoldCosts();
	UFUNCTION(BlueprintCallable, Category = Person)
	static int32 GetFoodCosts();
	
	/*
	 returns true if a given nation can afford to buy a person
	 \param nationX - nation to check
	 \return true if a person there are enough resources
	*/
	UFUNCTION(BlueprintCallable, Category = Person)
	static bool CanAfford(UNation *nationX);
	
	UFUNCTION(BlueprintCallable, Category = Person)
	static void Buy(UNation *nationX);
	
	/**
	* A Person needs to eat, this method fulfills this need :)
	* This method is static, because buildings don't really person object, so we need a way to consume food without an actual person
	* 
	* \param nation - persons nation
	*
	* \return returns true if the person starved to death
	*/
	static bool ConsumeFood(UNation *nationX);
	
	/**
	* Person gets hit with a given amount of power
	*/
	virtual void Attack(int32 power);
	
};
