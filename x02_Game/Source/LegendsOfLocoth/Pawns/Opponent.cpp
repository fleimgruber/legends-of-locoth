#include "LegendsOfLocoth.h"
#include "../Controller/NationAI.h"
#include "Opponent.h"



AOpponent::AOpponent(const FObjectInitializer& ObjectInitializer) : APawn(ObjectInitializer)
{
    AIControllerClass = ANationAI::StaticClass();
}


FString AOpponent::GetOpponentName(){
	return opponentName;
}