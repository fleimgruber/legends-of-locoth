#pragma once

#include "Pawns/Person.h"
#include "Soldier.generated.h"

class ABuilding;

UCLASS()
class LEGENDSOFLOCOTH_API ASoldier : public APerson
{
	GENERATED_BODY()
	
private:
	float timeToFollow, timeToAttack;
	
public:
	UPROPERTY(BlueprintReadWrite)
	APerson *targetPerson;
	UPROPERTY(BlueprintReadWrite)
	ABuilding *targetBuilding;
	
	ASoldier(const FObjectInitializer& ObjectInitialize);
	
	/*
	* returns an integer indicating the stength of this soldier
	*/
	virtual int32 GetPower(); //my power level is over 9000!
	
	/*
	* defines the soldiers speed, e.g. how long the delay between his attacks is (seconds)
	*/
	virtual float GetSpeed();
	
	/**
	* How close does the soldier need to be, in order to attack a person?
	*/
	virtual int32 GetRangePersons();
	
	/**
	 * How close does the soldier need to be, in order to attack a building?
	 */
	virtual int32 GetRangeBuildings();
	
	/**
	* Lets the soldier charge to the given person and attack it
	*/
	UFUNCTION(BlueprintCallable, Category=Soldier)
	void AttackPerson(APerson *person);
	
	/**
	 * tells the soldier to attack the given building
	 */
	UFUNCTION(BlueprintCallable, Category=Soldier)
	void AttackBuilding(ABuilding *building);

	/**
	 * Checks if the soldier is currently attacking a building or a person.
	*/
	UFUNCTION(BlueprintCallable, Category = Soldier)
	bool IsAttacking();
	
	/**
	* called every frame
	*/
	virtual void Tick(float delatT) override;
};
