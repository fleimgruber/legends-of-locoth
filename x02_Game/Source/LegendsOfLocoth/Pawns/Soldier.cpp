#include "LegendsOfLocoth.h"
#include "Soldier.h"
#include "../Buildings/Building.h"


ASoldier::ASoldier(const FObjectInitializer& ObjectInitialize) : APerson(ObjectInitialize)
{
	targetPerson = 0;
	targetBuilding = 0;
}

int32 ASoldier::GetPower()
{
	return 3;
}

float ASoldier::GetSpeed()
{
	return 1.5;
}

int32 ASoldier::GetRangePersons()
{
	return 500;
}

int32 ASoldier::GetRangeBuildings()
{
	return 800;
}

void ASoldier::AttackPerson(APerson *person)
{
	bool attackAllowed = true;
	//check for treaties
	for (UTreaty *treaty : nation->treaties){
		if ((treaty->sourceNation == person->nation && treaty->receiveNation == nation) || (treaty->sourceNation == nation && treaty->receiveNation == person->nation)){
			//check for non aggression pact
			if (Cast<UNonAggressionPact>(treaty)){
				attackAllowed = false;
				ALocothPlayerController *playerController = Cast<ALocothPlayerController>(GetWorld()->GetFirstPlayerController());
				playerController->ShowAttackForbiddenDialog("You cannot attack this person \nbecause you have a \nNon Aggression Pact \nwith their nation.");
			}
		}
	}
	if (attackAllowed){
		targetBuilding = 0;
		targetPerson = person;
		timeToFollow = 0;
		timeToAttack = 0;
	}	
}

void ASoldier::AttackBuilding(ABuilding *building)
{
	bool attackAllowed = true;
	//check for treaties
	for (UTreaty *treaty : nation->treaties){
		if ((treaty->sourceNation == building->nation && treaty->receiveNation == nation) || (treaty->sourceNation == nation && treaty->receiveNation == building->nation)){
			//check for non aggression pact
			if (Cast<UNonAggressionPact>(treaty)){
				attackAllowed = false;
				ALocothPlayerController *playerController = Cast<ALocothPlayerController>(GetWorld()->GetFirstPlayerController());
				playerController->ShowAttackForbiddenDialog("You cannot attack this building \nbecause you have a \nNon Aggression Pact \nwith its nation.");
			}
		}
	}
	if (attackAllowed){
		targetPerson = 0;
		targetBuilding = building;
		MoveToLocation(building->GetActorLocation());
		timeToAttack = 0;
	}	
}

void ASoldier::Tick(float deltaT)
{
	Super::Tick(deltaT);
	
	timeToAttack = FMath::Max(0.0f, timeToAttack - deltaT);
	
	if (targetPerson && !targetPerson->IsPendingKill())
	{
		//follow him
		timeToFollow -= deltaT;
		if (timeToFollow <= 0) {
			
			UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
			NavSys->SimpleMoveToActor(GetController(), targetPerson);
			timeToFollow = 1;
		}
		
		if ((GetActorLocation()-targetPerson->GetActorLocation()).Size() < GetRangePersons() && timeToAttack == 0)
		{
		//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "attack person");
			targetPerson->Attack(GetPower());
		}
	} else if (targetBuilding && !targetBuilding->IsPendingKill())
	{
		if ((GetActorLocation()-targetBuilding->GetActorLocation()).Size() < GetRangeBuildings() && timeToAttack == 0)
		{
		//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "attack building");
			targetBuilding->Attack(GetPower());
		}
	}
	
	if (timeToAttack == 0)
		timeToAttack = GetSpeed();
}

bool ASoldier::IsAttacking(){
	if (targetPerson == NULL && targetBuilding == NULL) return false;
	return true;
}

