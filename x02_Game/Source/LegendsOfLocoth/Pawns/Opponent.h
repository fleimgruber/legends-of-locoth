#pragma once

#include "GameFramework/Pawn.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Opponent.generated.h"


/**
 * This class is the link between the world and the controller
 *
 * \class AOpponent
 */
UCLASS()
class LEGENDSOFLOCOTH_API AOpponent : public APawn
{
	GENERATED_BODY()
	
public:
	AOpponent(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(EditAnywhere, Category = Behavior)
	UBehaviorTree *Behavior;
	
	UPROPERTY(EditAnywhere, Category = Opponent)
	FString opponentName;

	/**
	* Gets the opponent name
	*
	* \return opponent name
	*/
	UFUNCTION(BlueprintCallable, Category = Opponent)
	FString GetOpponentName();
};
