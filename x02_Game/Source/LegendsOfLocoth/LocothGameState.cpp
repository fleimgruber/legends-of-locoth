#include "LegendsOfLocoth.h"
#include "Controller/NationAI.h"
#include "Controller/LocothPlayerController.h"
#include "Controller/Nation.h"
#include "LocothGameState.h"
#include "BuildingResearch/BuildingResearch.h"


ALocothGameState::ALocothGameState(const FObjectInitializer& ObjectInitializer) : AGameState(ObjectInitializer)
{
    PrimaryActorTick.bCanEverTick = true;
	if (ALocothGameState::GetSlotName().Len() < 1) // only for testing
		ALocothGameState::SetSlotName(TEXT("test"));
	
	static ConstructorHelpers::FObjectFinder<UBlueprint> Villager(TEXT("/Game/levels/game/People/Villager"));
	if (Villager.Object)
		VillagerBP = (UClass*) Villager.Object->GeneratedClass;
	
	static ConstructorHelpers::FObjectFinder<UBlueprint> Soldier(TEXT("/Game/levels/game/People/Soldier"));
	if (Soldier.Object)
		SoldierBP = (UClass*) Soldier.Object->GeneratedClass;
	
}

void ALocothGameState::BeginPlay()
{
    Super::BeginPlay();
	
	UBuildingResearch::GetInstances().Empty();
	
    opponents.Empty();
    
    ALocothPlayerController *playerController = Cast<ALocothPlayerController>(GetWorld()->GetFirstPlayerController());
	playerController->Init();
    for (auto it=GetWorld()->GetPawnIterator(); it; it++)
    {
        AOpponent* opponent = Cast<AOpponent>(*it);
        if (opponent)
		{
            opponents.Add(opponent);
			Cast<ANationAI>(opponent->GetController())->Init();
		}
    }

	try
	{
		playerController->LoadGame(ALocothGameState::GetSlotName());
	} catch (...)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("You idiot, the save game file is corrupt!"));
	}
	
    
}

void ALocothGameState::Tick(float deltaT)
{
    Super::Tick(deltaT);
    lastDay+=deltaT;
    if (lastDay >= 1) //1 second per day
    {
        lastDay = 0;
        now++;
    }
}

bool ALocothGameState::IsNewDay(uint32 &myTime)
{
    bool r=now!=myTime;
    myTime=now;
    return r;
}

AOpponent *ALocothGameState::GetOpponent(int32 idx)
{
    return opponents[idx];
}

TArray<AOpponent*> ALocothGameState::GetOpponents()
{
    return opponents;
}

AOpponent *ALocothGameState::GetOpponentByName(FString name){
	for(auto opponent : opponents){
		if (opponent->GetOpponentName() == name) return opponent;
	}
	return NULL;
}

FString &ALocothGameState::GetSlotName()
{
	static FString slotName = TEXT("");
	return slotName;
}

void ALocothGameState::SetSlotName(FString value)
{
	GetSlotName() = value;
}