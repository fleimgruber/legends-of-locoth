// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "BuildingResearch.h"



UBuildingResearch::UBuildingResearch(const FObjectInitializer& ObjectInitialize) :UObject(ObjectInitialize)
{
	skills = TArray<USkill*>();
	efficiency = 1;
}

int32 UBuildingResearch::GetEfficiency()
{
	return efficiency;
}


void UBuildingResearch::AddEfficiency(int32 eff)
{
	efficiency+=eff;
}

void UBuildingResearch::SetEfficiency(int32 eff)
{
	efficiency = eff;
}

bool UBuildingResearch::AddSkill(USkill* skill)
{
	bool foundSkill = ContainsSkill(skill);
	if (!foundSkill)
		skills.Add(skill);
	return !foundSkill;
}

TArray<USkill*> &UBuildingResearch::GetSkills()
{
	return skills;
}

UBuildingResearch *UBuildingResearch::GetInstance(FString buildingString, int32 nationId)
{
	FString tmp = buildingString;
	buildingString.AppendInt(nationId);
	
	if (!GetInstances().Contains(buildingString))
	{
		GetInstances().Add(buildingString, NewObject<UBuildingResearch>());
		GetInstances()[buildingString]->name = tmp;
		GetInstances()[buildingString]->SetFlags(RF_RootSet);
	}
	return GetInstances()[buildingString];
}

TMap<FString, UBuildingResearch*> &UBuildingResearch::GetInstances()
{
	static TMap<FString, UBuildingResearch*> buildingResearches;
	return buildingResearches;
}

bool UBuildingResearch::ContainsSkill(USkill* skill)
{
	bool foundSkill = false;
	for (USkill* lSkill : skills)
	{
		if (skill->GetName().Equals(lSkill->GetName()))
		{
			foundSkill = true;
		}
	}
	return foundSkill;
}

TArray<USkill*> UBuildingResearch::GetAllSkills(int32 nationId)
{
	TArray<UBuildingResearch*> research = UBuildingResearch::GetAllResearch(nationId);
	TArray<USkill*> r;
	for (auto &res: research)
		for (auto &x : res->GetSkills())
			r.Add(x);
	return r;
}

TArray<UBuildingResearch*> UBuildingResearch::GetAllResearch(int32 nationId)
{
	FString id =  FString::FromInt(nationId);
	TArray<UBuildingResearch*> r;
	auto map = UBuildingResearch::GetInstances();
	for (auto &element : map)
		if (element.Key.EndsWith(id))
			r.Add(element.Value);
	return r;
}