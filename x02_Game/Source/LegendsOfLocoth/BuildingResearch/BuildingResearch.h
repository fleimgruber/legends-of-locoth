// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Map.h"
#include "BuildingResearch.generated.h"

class USkill;

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UBuildingResearch : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly)
	FString name;

	UPROPERTY()
	int32 efficiency;

	UPROPERTY()
	TArray<USkill*> skills;

	TArray<USkill*> &GetSkills();


	UBuildingResearch(const FObjectInitializer& ObjectInitialize);

	/**
	* Returns the efficiency for building type/nation.
	*
	* \return efficiency as int32
	*/
	int32 GetEfficiency();

	/**
	* Adds the efficiency to the current efficiency.
	*
	* \param eff is the efficiency to add.
	*/
	void AddEfficiency(int32 eff);

	/**
	* Sets the efficiency to the given value.
	*
	* \param eff is the efficiency to set
	*/
	void SetEfficiency(int32 eff);

	/**
	* Adds the skill if it is not already added.
	*
	* \param skill the skill to add.
	*
	* \return Wether or not the skill was added successfully.
	*/
	bool AddSkill(USkill* skill);

	/**
	* This method hands out instances based on the buildingString and the nationId
	*
	* \param buildingString a string (based on the names in the building classes)
	* \param nationId the nationId of the nation.
	*
	* \return The Instance
	*/
	static UBuildingResearch *GetInstance(FString buildingString, int32 nationId);
	
	/**
	 * This method hands out all instances
	 *
	 * \return map of instances
	 */
	static TMap<FString, UBuildingResearch*> &GetInstances();
	
	/**
	* Returns a list of all skills (that is, for all buildings combined) of a given nation
	*/
	UFUNCTION(BlueprintCallable, Category = Research)
	static TArray<USkill*> GetAllSkills(int32 nationId);

	/**
	* Returns all UBuildingsResearch instances for a given nation
	*/
	UFUNCTION(BlueprintCallable, Category = Research)
	static TArray<UBuildingResearch*> GetAllResearch(int32 nationId);

	/**
	* Checks wether the skill is already added or not.
	*
	* \param skill the skill to check
	*
	* \return Wether the skill is already added or not.
	*/
	bool ContainsSkill(USkill* skill);
};
