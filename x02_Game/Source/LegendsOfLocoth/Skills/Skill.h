#pragma once

#include "Object.h"
#include "../Controller/Nation.h"
#include "../BuildingResearch/BuildingResearch.h"
#include "Skill.generated.h"

UCLASS()
class LEGENDSOFLOCOTH_API USkill : public UObject
{
	GENERATED_BODY()
	

protected:



public:
	USkill(const FObjectInitializer& ObjectInitialize);

	/**
	* This method updates the efficiency for a given nation in all the concerned buildings.
	* Does nothing, implemented in child classes.
	*
	* \param nationId is the id of the nation this skill gets researched for
	*/
	virtual bool UpdateSettings(int32 nationId);

	/**
	* Returns the propability (a whole number) that this skill can be researched.
	* Returns 0, because this is just the base class.
	*
	* \return The propability as a whole number
	*/
	virtual int32 GetPropability();

	UPROPERTY(BlueprintReadOnly)
		FString name;


	/**
	* Returns the name of the building
	*
	* \return The name of the Building
	*/
	virtual FString GetName();
	
};
