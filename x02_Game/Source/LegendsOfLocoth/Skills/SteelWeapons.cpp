// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "SteelWeapons.h"



USteelWeapons::USteelWeapons(const FObjectInitializer& ObjectInitialize) :USkill(ObjectInitialize)
{
	name = TEXT("Steel Weapons");
}

bool USteelWeapons::UpdateSettings(int32 nationId)
{
	return false;
}

int32 USteelWeapons::GetPropability()
{
	return 12;
}

FString USteelWeapons::GetName()
{
	return name;
}