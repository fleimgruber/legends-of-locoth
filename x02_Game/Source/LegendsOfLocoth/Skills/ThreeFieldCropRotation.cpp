// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "ThreeFieldCropRotation.h"
#include "../Buildings/Farm.h"



UThreeFieldCropRotation::UThreeFieldCropRotation(const FObjectInitializer& ObjectInitialize) :USkill(ObjectInitialize)
{
	name = TEXT("Three Field Crop Rotation");
}

bool UThreeFieldCropRotation::UpdateSettings(int32 nationId)
{
	UBuildingResearch* res = UBuildingResearch::GetInstance(AFarm::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(10);
	else
		return false;
	return true;
}

int32 UThreeFieldCropRotation::GetPropability()
{
	return 9;
}

FString UThreeFieldCropRotation::GetName()
{
	return name;
}