#include "LegendsOfLocoth.h"
#include "Skill.h"




USkill::USkill(const FObjectInitializer& ObjectInitialize) :UObject(ObjectInitialize)
{
	name = "untitled base skill";
}

bool USkill::UpdateSettings(int32 nationId) {/*to be really implemented in the child classes*/ return true; }

int32 USkill::GetPropability()
{
	return 0;
}

FString USkill::GetName()
{
	return name;
}