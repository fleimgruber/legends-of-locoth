// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "GoDeeper.h"
#include "../Buildings/GoldMine.h"



UGoDeeper::UGoDeeper(const FObjectInitializer& ObjectInitialize) :USkill(ObjectInitialize)
{
	name = TEXT("We Need To Go Deeper");
}

bool UGoDeeper::UpdateSettings(int32 nationId)
{
	UBuildingResearch* res = UBuildingResearch::GetInstance(AGoldMine::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(20);
	else
		return false;
	return true;
}

int32 UGoDeeper::GetPropability()
{
	return 1;
}

FString UGoDeeper::GetName()
{
	return name;
}