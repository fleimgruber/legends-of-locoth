// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "Crystals.h"
#include "../Buildings/GoldMine.h"

UCrystals::UCrystals(const FObjectInitializer& ObjectInitialize) :USkill(ObjectInitialize)
{
	name = TEXT("Crystals");
}

bool UCrystals::UpdateSettings(int32 nationId)
{
	UBuildingResearch* res = UBuildingResearch::GetInstance(AGoldMine::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(3);
	else
		return false;
	return true;
}

int32 UCrystals::GetPropability()
{
	return 4;
}

FString UCrystals::GetName()
{
	return name;
}