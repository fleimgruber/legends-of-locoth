// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "SteelTools.h"
#include "../Buildings/GoldMine.h"
#include "../Buildings/StoneQuarry.h"
#include "../Buildings/WoodCutter.h"


USteelTools::USteelTools(const FObjectInitializer& ObjectInitialize) :USkill(ObjectInitialize)
{
	name = TEXT("Steel Tools");
}

bool USteelTools::UpdateSettings(int32 nationId)
{
	UBuildingResearch* res = UBuildingResearch::GetInstance(AGoldMine::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(5);
	else
		return false;
	res = UBuildingResearch::GetInstance(AStoneQuarry::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(7);
	res = UBuildingResearch::GetInstance(AWoodCutter::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(10);
	return true;
}

int32 USteelTools::GetPropability()
{
	return 11;
}

FString USteelTools::GetName()
{
	return name;
}