// Fill out your copyright notice in the Description page of Project Settings.

#include "LegendsOfLocoth.h"
#include "BetterScythe.h"
#include "../Buildings/Farm.h"


UBetterScythe::UBetterScythe(const FObjectInitializer& ObjectInitialize) :USkill(ObjectInitialize)
{
	name = TEXT("Better Scythe");

}

bool UBetterScythe::UpdateSettings(int32 nationId)
{
	UBuildingResearch* res = UBuildingResearch::GetInstance(AFarm::GetNameStatic(), nationId);
	if (res->AddSkill(this))
		res->AddEfficiency(5);
	else
		return false;
	return true;
}

int32 UBetterScythe::GetPropability()
{
	return 7;
}

FString UBetterScythe::GetName()
{
	return name;
}