// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Skills/Skill.h"
#include "BetterScythe.generated.h"

/**
 * 
 */
UCLASS()
class LEGENDSOFLOCOTH_API UBetterScythe : public USkill
{
	GENERATED_BODY()
	

public:
	UBetterScythe(const FObjectInitializer& ObjectInitialize);

	/**
	* This method updates the efficiency for a given nation in all the concerned buildings.
	* Affects only the farm
	*
	* \param nationId is the id of the nation this skill gets researched for
	*/
	virtual bool UpdateSettings(int32 nationId) override;

	/**
	* Returns the propability (a whole number) that this skill can be researched.
	*
	* \return The propability as a whole number
	*/
	virtual int32 GetPropability() override;

	/**
	* Returns the name of the building
	*
	* \return The name of the Building
	*/
	virtual FString GetName() override;
};
